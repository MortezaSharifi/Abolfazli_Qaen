<?php
/**
 * Created by PhpStorm.
 * User: salari
 * Date: 9/14/2017
 * Time: 7:35 PM
 */

namespace App;
use Illuminate\Database\Eloquent\Model;

class gallery extends model
{
    protected $table='gallery';
    public $timestamps = false;
    protected $fillable = [
        'id', 'name','file_type', 'file_id', 'caption','group'
    ];


}