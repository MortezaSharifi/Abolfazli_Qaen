<?php
/**
 * Created by PhpStorm.
 * User: Shadow
 * Date: 18/08/2017
 * Time: 10:28 AM
 */

namespace App\Http\Controllers\Admin\Messages;


use App\driver\SysComment;
use App\Http\Controllers\Base\Base;
use App\Utils\ValidationForm;
use Illuminate\Http\Request;
use App\Libraries;

class UserMessage extends Base
{

    function __construct(Request $request)
    {
        $type = Base::get_user_type();
        parent::__construct($request, $type, 'ADMIN');
    }

    public function get()
    {
        include(app_path() . '\Libraries\jdf.php');

        $user_msg = new SysComment(null, null, null, null, 'F');
        $msgs = $user_msg->get_user_msg();
        for ($i = 0; $i < count($msgs); $i++) {
            $msgs[$i]['date'] = Libraries\jdate(" i : H j F Y", strtotime($msgs[$i]['date']));
        }
        return view('admin.messages', compact('msgs'));

    }

    public function delete()
    {
        $comment_id = (int)$this->request->input('comment_id');

        $sysComment = new SysComment($comment_id);
        $deleteRow = $sysComment->deleteRecord();
        if ($deleteRow === false) {
            return json_encode([
                "status" => 'error',
                "message" => 'پیام حذف نشد مجددا تلاش کنید'
            ]);

        }

        return json_encode([
            "status" => 'success',
            "message" => 'پیام با موفقیت حذف شد!'
        ]);
    }

    public function post()
    {

        $replay = $this->request->input('reply');
        $replay = trim($replay);
        $user_id = $this->request->input('user_id');

        $valid = new ValidationForm();
        if ($valid->requiredField($replay)) {
            $obj_comment = new SysComment(null, $replay, null, $user_id, 'T');
            $obj_comment->insert();
            return json_encode([
                "status" => 'success',
                "message" => 'پیام ارسال شد'
            ]);
        }
        return json_encode([
            "status" => 'error',
            "message" => 'متنی وارد نشده است'
        ]);

    }

    public function lastMessages()
    {

        include(app_path() . '\Libraries\jdf.php');

        $user_msg = new SysComment(null, null, null, null, 'F');
        $msgs = $user_msg->get_three_msg();
        if ($msgs!= []) {
            for ($i = 0; $i < count($msgs); $i++) {
                $msgs[$i]['date'] = Libraries\jdate(" i : H j F Y", strtotime($msgs[$i]['date']));
            }
            for ($i = 0; $i < count($msgs); $i++) {
                $username[$i] = $msgs[$i]['username'];
                $date[$i] = $msgs[$i]['date'];
                $text[$i] = $msgs[$i]['text'];
            }

            return json_encode([
                "username" => $username,
                "date" => $date,
                "text" => $text
            ]);
        } else {
            return json_encode([
                "status" => 'error'
            ]);
        }
    }
}