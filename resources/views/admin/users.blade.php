@extends('base.admin_base')
@section('title','اعضا')
@section('css')
    <link href="{{URL::asset('css/plugins/dataTables.bootstrap.css')}}" rel="stylesheet">
@endsection
@section('users_nav')
    class = "active"
@stop
@section('body')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                            <tr>
                                <th>نام</th>
                                <th>نام خانوادگی</th>
                                <th>تلفن</th>
                                <th>آدرس</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($all_users as $user)
                                <tr>
                                    <td>{{$user->fname}}</td>
                                    <td>{{$user->lname}}</td>
                                    <td>{{$user->phone}}</td>
                                    <td>{{$user->address}}</td>

                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->


                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="{{url::asset('js/jquery/jquery.dataTables.min.js')}}"></script>
    <script src="{{url::asset('js/bootstrap/dataTables.bootstrap.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('#dataTables-example').dataTable();
        });
    </script>
@endsection