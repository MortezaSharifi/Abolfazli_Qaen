<?php

/**
 * Created by PhpStorm.
 * User: Shadow
 * Date: 25/07/2017
 * Time: 01:58 PM
 */
namespace App;

use Illuminate\Database\Eloquent\Model;

class Multimedia extends Model
{
    protected $table = 'multimedia';
    public $timestamps = false;
    protected $fillable = [
        'id',
        'name',
        'content_id',
        'file_id',
        'content_type',
    ];

}

