<?php
/**
 * Created by PhpStorm.
 * User: Shadow
 * Date: 13/08/2017
 * Time: 12:30 PM
 */

namespace App\Http\Controllers\Site\Profile;


use App\driver\SysComment;
use App\driver\SysPayment;
use App\driver\SysUsers;
use App\Http\Controllers\Base\Base;
use Illuminate\Http\Request;
use App\Utils\ValidationForm;
use App\Libraries;

class ProfileController extends Base
{
    function __construct(Request $request)
    {
        $type = Base::get_user_type();
        parent::__construct($request, $type, 'MEMBER');
    }

    public function get()
    {
        include(app_path() . '\Libraries\jdf.php');
        $type = Base::get_user_type();
        $user_info = Base::get_current_user();
        $payment = new SysPayment(null, null,null,$user_info['id']);
        $details = $payment->get_user_pay();
        for ($i = 0; $i < count($details); $i++){
            $details[$i]['date'] = Libraries\jdate(" i : H j F Y", strtotime($details[$i]['date']));

            if ($details[$i]['type'] == 'n')
                $details[$i]['type'] = 'نذورات';
            if ($details[$i]['type'] == 'k')
                $details[$i]['type'] = 'کمک به بازسازی';
            if ($details[$i]['type'] == 'g')
                $details[$i]['type'] = 'جشن';
            if ($details[$i]['type'] == 't')
                $details[$i]['type'] = 'ترحیم';
            if ($details[$i]['type'] == 's')
                $details[$i]['type'] = 'سایر';

        }

        $user_msg = new SysComment(null, null, null, $user_info['id']);
        $msgs = $user_msg->get_msg();
        for ($i = 0, $len = count($msgs); $i < $len; $i++) {
            $msgs[$i]['date'] = Libraries\jdate("j F Y i : H", strtotime($msgs[$i]['date']));
        }
        //$msgs[ $len - 1 ]['date'] = Libraries\jdate("j F Y i : H", strtotime($msgs[ $len - 1 ]['date']));
        return view('site.profile', compact('user_info', 'msgs','type', 'details'));

    }

    public function post()
    {

        $fname = $this->request->input('fname');
        $lname = $this->request->input('lname');
        $username = $this->request->input('username');
        $phone = $this->request->input('phone');
        $email = $this->request->input('email');
        $address = $this->request->input('address');
        $address = trim($address);

        $valid = new ValidationForm();
        $err = [];
        if (!$valid->req_alpha_Field($fname)) {
            $err["E_fname"] = 'نام خود را فارسی وارد کنید';
        } else $err["E_fname"] = '';
        if (!$valid->req_alpha_Field($lname)) {
            $err["E_lname"] = 'نام خانوادگی خود را فارسی وارد کنید';
        } else $err["E_lname"] = '';
        if (!$valid->requiredField($username)) {
            $err["E_username"] = 'نام کاربری را وارد کنید';
        } else $err["E_username"] = '';
        if (!$valid->checkEmail($email)) {
            $err["E_email"] = 'ایمیل درست وارد نشده است';
        } else $err["E_email"] = '';

        $user_id = $this->request->session()->get('userid');

        if ($valid->req_alpha_Field($fname)
            and $valid->req_alpha_Field($lname) and $valid->requiredField($username) and $valid->checkEmail($email)
        ) {
//            $this->request->session()->put('username',$username);
            $obj_user = new SysUsers($user_id, $username, $fname, $lname, $phone, $email, $address);
            $user = $obj_user->updateRecord();
            if ($user != false) {
                $this->request->session()->put('username', $username);
                return json_encode([
                    "status" => 'success',
                    "message" => 'اطلاعات با موفقیت ویرایش شد'
                ]);
            } else {
                $err['E_exist_user'] = 'این نام کاربری از قبل وجود دارد ';
            }



        }

        return json_encode([
            "status" => 'error',
            "message" => $err
        ]);
    }

    public function changePass()
    {
        $password = $this->request->input('password');
        $valid = new ValidationForm();
        $new_pass = $this->request->input('new_pass');
        $conf_pass = $this->request->input('conf_pass');


        if (!$valid->requiredField($password)) {
            $err['E_password'] = 'گذرواژه را وارد کنید';
        } else $err['E_password'] = '';
        if (!$valid->requiredField($new_pass)) {
            $err['E_new_pass'] = 'گذرواژه جدید را وارد کنید';
        } else $err['E_new_pass'] = '';
        if (!$valid->requiredField($conf_pass)) {
            $err['E_conf_pass'] = 'تایید گذرواژه را وارد کنید';
        } else $err['E_conf_pass'] = '';
        if (!$valid->matchPassword($new_pass, $conf_pass)) {
            $err['E_match'] = 'تایید گذرواژه با گذرواژه مطابقت ندارد';
        } else $err['E_match'] = '';

        if ($valid->requiredField($password) and $valid->requiredField($new_pass)
            and $valid->requiredField($conf_pass) and $valid->matchPassword($new_pass, $conf_pass)){
            $user_id = $this->request->session()->get('userid');
            $obj_user = new SysUsers($user_id);

            $user = $obj_user->getOne();
            $hdb = $user['password'];
            $username = $user['username'];

            if (password_verify($password, $hdb)){
                $new_hash = password_hash($new_pass, PASSWORD_DEFAULT);
                $obj_user = new SysUsers($user_id, null, null, null,
                    null,null, null, $new_hash);
                $obj_user->updateRecord();

                return json_encode([
                    "status" => 'success',
                    "message" => 'گذرواژه با موفقیت تغییر کرد'
                ]);
            }else $err['E_pass_incorrect'] = 'گذرواژه وارد شده اشتباه است';

        }


        return json_encode([
            "status" => 'error',
            "message" => $err
        ]);
    }
}