<?php

/**
 * Created by PhpStorm.
 * User: salari
 * Date: 7/12/2017
 * Time: 4:31 PM
 */

namespace App\driver;

use App\gallery;
use Illuminate\Database\Eloquent\Model;
use Mockery\Exception;
use Illuminate\Database\QueryException;
use App\Libraries;


class SysGallery
{
    private $id;
    private $name;
    private $file_type;
    private $file_id;
    private $caption;
    private $group;



    public function __construct($id = null, $name = null, $file_type = null, $file_id = null, $caption = null,
                                $group = null)
    {

        $this->id = $id;
        $this->name = $name;
        $this->file_type = $file_type;
        $this->file_id = $file_id;
        $this->caption = $caption;
        $this->group = $group;


    }

    public function getAll()
    {
        try {
            return gallery::all();
        } catch (Exception $e) {
            return [];
        }

    }

    public function getOne()
    {
        try {
            return gallery::all()->where('id', $this->id);
        } catch (Exception $e) {
            return [];
        }

    }

    public function insert()
    {
        try {
            $gallery = new gallery();
            $gallery->name = $this->name;
            $gallery->file_id = $this->file_id;
            $gallery->file_type = $this->file_type;
            $gallery->group = $this->group;
            $gallery->caption = $this->caption;


            $gallery->save();

            return true;
        } catch (Exception $e) {
            return false;
        }

    }

    public function deleteRecord()
    {
        try {
            $gallery = gallery::find($this->id);

            $gallery->delete();
            return true;
        } catch (Exception $e) {
            return false;
        }

    }

    public function updateRecord()
    {
        try {
            $gallery = gallery::find($this->id);

            $gallery->save();


            return true;
        } catch (Exception $e) {
            return false;
        }

    }
}