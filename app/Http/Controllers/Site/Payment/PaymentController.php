<?php

namespace App\Http\Controllers\Site\Payment;

use App\driver\SysCost;
use App\driver\SysPayment;
use App\driver\SysUsers;
use App\Http\Controllers\Base\Base;
use App\Http\Controllers\Controller;
use App\payment;
use App\Utils\ValidationForm;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    private $request;

    function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function get()
    {
        $type = Base::get_user_type();
        return view('site.payment', compact('type'));
    }


    public function getCost()
    {
        $cost = new Syscost();
        $costs = $cost->getAll();

        if (!$costs->isEmpty()) {
            return json_encode([
                "status" => 'success',
                "costs" => $costs
            ]);
        }
        return json_encode([
            "status" => 'error'
        ]);
    }



    public function post()
    {

        $fname = $this->request->input('fname');
        $lname = $this->request->input('lname');
        $type = $this->request->input('type');
        $text = $this->request->input('text');
        $text = trim($text);
        $amount = $this->request->input('amount');

        $valid = new ValidationForm();

        if ($valid->requiredField($amount)) {
            $err['E_amount'] = '';
            $user_id = $this->request->session()->get('userid');

            if ($user_id == null) {
                $obj_user = new SysUsers(null, 'ادمین');
                $user = $obj_user->get_by_uname();
                $user_id = $user['id'];
                if ($fname == '')
                    $fname = 'ناشناس';
                if ($lname == '')
                    $lname = 'ناشناس';
                $obj_payment = new SysPayment(null, null, $amount, $user_id, $type, $text, $fname, $lname);
                $obj_payment->insert();
                return json_encode([
                    "status" => 'success',
                    "message" => $user_id
                ]);
            } else {
                $user_info = Base::get_current_user();
                $fname = $user_info['fname'];
                $lname = $user_info['lname'];
                $obj_payment = new SysPayment(null, null, $amount, $user_id, $type, $text, $fname, $lname);
                $obj_payment->insert();
                return json_encode([
                    "status" => 'success',
                    "message" => $user_id
                ]);
            }


        } else $err['E_amount'] = 'لطفا مبلغ را وارد کنید';

        return json_encode([
            "status" => 'error',
            "message" => $err
        ]);
    }


}