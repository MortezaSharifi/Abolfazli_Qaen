@extends('base.admin_base')
@section('title','آپلود عکس اسلایدر')
@section('css')
    <link href="{{ URL::asset("plugin/dropzone/min/dropzone.min.css") }}" rel="stylesheet" type="text/css"/>
@endsection
@section('up_slider_nav')
    class = "active"
@endsection
@section('body')

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div>
                        <div class="form-group">
                            <label for="caption">عنوان</label>
                            <input type="text" name="title" class="form-control" id="caption"
                                   placeholder="عنوان را وارد کنید ...">
                        </div>

                        <form enctype="multipart/form-data" method="post" style="margin: 15px 0;" id='my_dropzone'
                              class="dropzone dz-clickable">
                            <div class="dz-default dz-message"><span>فایل را از سیستم انتخاب یا بکشید و رها کنید</span>
                            </div>
                        </form>


                        <div name="reset" class="btn btn-danger">پاک کردن</div>
                        <div class="btn btn-success" id="send"> ثبت</div>

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{ URL::asset('plugin/dropzone/min/dropzone.min.js') }}"></script>
    <script type="text/javascript">
        var ls_file_ids = [];

        function empty() {

            $('input[type="text"]').val('');
            ls_file_ids = [];
            $('.dz-preview').remove();
        }

        function setListFileId(data) {
            ls_file_ids.push(data);
        }

        function getLitFileID() {
            return JSON.stringify(ls_file_ids);
        }
        // dropzone
        var $dropzone = $('#my_dropzone');

        Dropzone.autoDiscover = false;
        $dropzone.dropzone({
            url: "{{action('Admin\Post\UploadController@post')}}",
            paramName: "file",
            addRemoveLinks: true,
            dictDefaultMessage: '<span class="text-center"><span class="font-lg visible-xs-block visible-sm-block visible-lg-block"><span class="font-lg"><i class="fa fa-caret-right text-danger"></i> Drop files <span class="font-xs">to upload</span></span><span>&nbsp&nbsp<h4 class="display-inline"> (Or Click)</h4></span>',
            dictResponseError: 'آپلود فایل با خطا مواجه شد.',
            dictFallbackMessage: 'مروگروب شما قابلیت کشیدن فایل و انداختن آن را پشتیبانی نمی کند.',
            parallelUploads: 1,
            dictCancelUpload: 'لغو آپلود',
            dictRemoveFile: 'حذف فایل',
            dictCancelUploadConfirmation: 'آیا شما مطمئن هستید از لغو آپلود فایل ؟',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                "Cache-Control": "",
                "X-Requested-With": ""
            },
            removedfile: function (_file) {
                var $fileElement = $(_file['previewElement']);
                var fileId = $fileElement.find('a').attr('data-id');
                var _token = $('meta[name="_token"]').attr('content');
                // delete file
                $.ajax({
                    type: 'POST',
                    url: "{{action('Admin\Post\DeleteFileController@post')}}",
                    data: {"fileId": fileId, "_token": _token},
                    success: function (_ret) {
                        var ret = JSON.parse(_ret);
                          $fileElement.remove();
                        //delete from ls-file-ids
                        ls_file_ids = $.grep(ls_file_ids, function(e) { return e.fileId!==fileId });
                        console.log(ls_file_ids)
//                        delete ls_file_ids[];

                    },
                    error: function (e) {
                        $fileElement.remove();

                    }
                })

            },
            success: function (_file, _ret) {
                var ret = JSON.parse(_ret);

                if (ret['status'] === 'success') {
                    var fileName = ret['fileName'];
                    var fileId = ret['fileId'];
                    var contentType = ret['contentType'];

                    // add file id to remove link
                    $(_file['previewElement']).find('a').attr('data-id', fileId.toString());

                    var file = {"fileName": fileName, "fileId": fileId, "contentType": contentType};
                    setListFileId(file);
                } else {
                    console.log('file not allowed !!!');
                    var $fileElement = $(_file['previewElement']);
                    $fileElement.remove();

                }

            }
        });


        $(document).on('click', '#send', function () {
            var notification = null;
            var news = null;
            var gallery = null;
            if (document.getElementById('notification').checked) {
                notification = "notification";
            }
            if (document.getElementById('news').checked) {
                news = "news";
            }
            if (document.getElementById('gallery').checked) {
                gallery = "gallery";
            }

            var caption = $('#caption').val();
            var files = getLitFileID();
            var _token = $('meta[name="_token"]').attr('content');


            $.ajax({
                type: 'POST',
                url: "{{action('Admin\Post\ContentController@post')}}",
                data: {
                    "files": files,
                    "caption": caption,
                    "_token": _token
                },
                success: function (_ret) {
                    var ret = JSON.parse(_ret);
                    console.log(ret);

                    alert(ret['message']);
                    empty();

                },
                error: function (e) {
                    console.log(e)
                }
            })

        })


    </script>
@endsection