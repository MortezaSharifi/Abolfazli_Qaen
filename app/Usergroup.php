<?php
/*
* @property null type
 *
 */
namespace App;

use Illuminate\Database\Eloquent\Model;

class Usergroup extends Model
{
    protected $table = 'user_group';
    public $timestamps = false;
    protected $fillable= ['type'];

}
