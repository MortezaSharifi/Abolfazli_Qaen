<?php

namespace App\Http\Controllers\Admin\Post;

use App\Http\Controllers\Controller;
use App\driver\SysContent;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Utils\Upload;
use App\Utils\MimeType;


class UploadController extends Controller
{

    public function post(Request $request)
    {
        if (!empty($_FILES['file'])) {

            $upload = Upload::factory(env('UPLOAD_PATH', 'public/uploaded'));
            $upload->file($_FILES['file']);

            // set max file size 500MB default
            $upload->set_max_file_size((int)env('MAX_UPLOAD_SIZE', 500));

            // set allowed MimeType
            $upload->set_allowed_mime_types(array_merge(MimeType::$images, MimeType::$audios, MimeType::$videos));

            $results = $upload->upload();

            if ($results['status'] == true) {

                // return file name and file id
                return json_encode([
                    "status" => 'success', "fileName" => $results['original_filename'],
                    "fileId" => $results['filename'], "contentType" => $results['mime']
                ]);

            }

        }


        return json_encode(["status" => 'error']);
    }

    public function     post_custom(Request $request){
        if (!empty($_FILES['file'])) {

            $upload = Upload::factory(env('UPLOAD_PATH', 'public/uploaded'));
            $upload->file($_FILES['file']);

            // set max file size 500MB default
            $upload->set_max_file_size((int)env('MAX_UPLOAD_SIZE', 500));

            // set allowed MimeType
            $upload->set_allowed_mime_types(array_merge(MimeType::$images));

            $results = $upload->upload();

            if ($results['status'] == true) {

                // return file name and file id
                return json_encode([
                    "status" => 'success', "fileName" => $results['original_filename'],
                    "fileId" => $results['filename'], "contentType" => $results['mime']
                ]);

            }

        }


        return json_encode(["status" => 'error']);
    }


}