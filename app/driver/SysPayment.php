<?php

/**
 * Created by PhpStorm.
 * User: salari
 * Date: 7/12/2017
 * Time: 4:31 PM
 */

namespace App\driver;

use App\Payment;
use Illuminate\Database\QueryException;
use Mockery\Exception;


class SysPayment
{
    private $id;
    private $date;
    private $value;
    private $users_id;
    private $type;
    private $text;
    private $fname;
    private $lname;
    private $transaction_id;
    private $status;


    public function __construct($id = null, $date = null, $value = null, $users_id = null, $type = null,
                                $text = null,$fname = null, $lname = null, $transaction_id = null, $status = null)
    {

        $this->id = $id;
        $this->date = $date;
        $this->value = $value;
        $this->users_id = $users_id;
        $this->type = $type;
        $this->text = $text;
        $this->fname = $fname;
        $this->lname = $lname;
        $this->transaction_id = $transaction_id;
        $this->status = $status;

    }

    public function getAll()
    {
        try {
            return Payment::select('*')->orderBy('date', 'desc')->get();
        } catch (Exception $e) {
            return [];
        }

    }

    public function getOne()
    {
        try {
            return Payment::all()->where('id', $this->id);
        } catch (Exception $e) {
            return [];
        }

    }

    public function get_user_pay(){
        try{
            return Payment::where('users_id', '=', $this->users_id)->get();
        }catch (QueryException $e){
            return [];
        }
    }
    public function total_pay(){
        try{
            return Payment::select('value')->sum('value');
        }catch (QueryException $e){
            return [];
        }
    }
    public function five_last(){
        try{
            return Payment::select('*')->orderBy('date', 'desc')->take(5)->get();
        }catch (QueryException $e){
            return [];
        }
    }

    public function insert()
    {
        try {
            $payment = new Payment();
            $payment->value = $this->value;
            $payment->users_id = $this->users_id;
            $payment->type = $this->type;
            $payment->text = $this->text;
            $payment->fname = $this->fname;
            $payment->lname = $this->lname;
            $payment->transaction_id = $this->transaction_id;
            $payment->status = $this->status;

            $payment->save();

            return true;
        } catch (QueryException $e) {
            return false;
        }

    }

    public function deleteRecord()
    {
        try {
            $payment = Payment::find($this->id);

            $payment->delete();
            return true;
        } catch (Exception $e) {
            return false;
        }

    }

    public function updateRecord()
    {
        try {
            $payment = Payment::find($this->id);

            if (isset($this->value)) {
                $payment->value = $this->value;
            }
            if (isset($this->users_id)) {
                $payment->users_id = $this->users_id;
            }
            if (isset($this->type)) {
                $payment->type = $this->type;
            }
            if (isset($this->text)) {
                $payment->text = $this->text;
            }
            if (isset($this->transaction_id)) {
                $payment->transaction_id = $this->transaction_id;
            }
            if (isset($this->status)) {
                $payment->status = $this->status;
            }


            $payment->save();


            return true;
        } catch (Exception $e) {
            return false;
        }

    }
}