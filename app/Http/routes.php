

<?php

use App\driver\SysGallery;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['prefix' => 'admin'], function () {
    Route::get('upslider', function () {
        return view('admin.SliderUploud');
    });
    Route::get('message', 'Admin\Messages\UserMessage@get');
    Route::post('message', 'Admin\Messages\UserMessage@post');
    Route::post('message/latest', 'Admin\Messages\UserMessage@lastMessages');
    Route::delete('message', 'Admin\Messages\UserMessage@delete');

    Route::get('sms', 'Admin\Messages\SmsController@get');
    Route::post('sms', 'Admin\Messages\SmsController@post');


    Route::get('/posts/get', 'Admin\Post\ContentController@get');

    Route::get('showcontent', function () {
        return view('admin.ShowContent');
    });

    Route::get('dashboard', 'Admin\Dashboard\DashboardController@get');

    Route::get('report', 'Admin\Reports\ReportController@get');

    Route::get('/users', 'Admin\User\UserController@get');

    Route::get('/changepass', 'Admin\ChangePass\ChangePassController@get');

    Route::post('changepass', 'Admin\ChangePass\ChangePassController@post');

    Route::get('cost', 'Admin\Cost\CostController@get');
    Route::post('cost', 'Admin\Cost\CostController@post');


});

Route::get('register', 'Site\Register\RegisterController@get');
Route::post('register', 'Site\Register\RegisterController@post');

//Route::get('login', 'Site\LoginController@get');
Route::post('login', 'Site\LoginController@post');
Route::post('logout', 'Site\LoginController@logout');


//Route::get('/',"Site\HomeController@get");
Route::get('/index/{page_num}',"Site\HomeController@get");
Route::get('/admin/showcontent',"Site\HomeController@getAllContent");

/******detail******/
Route::get('news/{content_id}', 'Site\HomeController@getNews');
/******end detail*****/

Route::get('profile', 'Site\Profile\ProfileController@get');
Route::post('profile', 'Site\Profile\ProfileController@post');
Route::post('profile/changepass', 'Site\Profile\ProfileController@changePass');
Route::post('profile/comment', 'Site\Profile\ProfileMsgController@post');

Route::get('payment', 'Site\Payment\PaymentController@get');
Route::post('payment', 'Site\Payment\PaymentController@post');

Route::get('gallery', function () {
        $ob = new SysGallery();
        $listGallery = $ob->getAll();

        return view('site.gallery', ['type'=>null], compact('listGallery'));
    });
Route::get('gallery/show', function () {
        return view('site.show',['type'=>null]);
    });

//Route::get('login', function () {
//    return view('login');
//});

// end test urls }
//Usergroup urls
Route::get('/Usergroup', 'Usergroup\UsergroupController@get');
Route::post('/Usergroup/post', 'Usergroup\UsergroupController@post');
//-------------
//post url
Route::get('/posts/get', 'Admin\Post\ContentController@get');
Route::post('/posts/post', 'Admin\Post\ContentController@post');
Route::post('/posts/delete1', 'Admin\Post\ContentController@delete');
//end post url
//--------------
//Type_post url
Route::get('/typepost/get', 'Typepost\TypepostController@get');
Route::post('/typepost/post', 'Typepost\TypepostController@post');
//end type_post
//payment url
//---------------
//payment url
Route::get('/Payment/get', 'Payment\PaymentController@get');
Route::post('/Payment/post', 'Payment\PaymentController@post');
Route::post('/Payment/getcost', 'Site\Payment\PaymentController@getCost');
//end url
//----------------
//Cost_has_payment url
Route::get('/costhaspayment/get', 'Costhaspayment\CosthaspaymentController@get');
Route::post('/costhaspayment/post', 'Costhaspayment\CosthaspaymentController@post');
//end url
//----------------
//Cost url
Route::get('/Cost/get', 'Cost\CostController@get');
Route::post('/Cost/post', 'Cost\CostController@post');
//End url


// Author Morteza
Route::get('/content/download/{fileId}', 'Admin\Post\DownloadController@get');
Route::post('/content/upload/', 'Admin\Post\UploadController@post');
Route::post('/content/upload/', 'Admin\Post\UploadController@post_custom');
Route::post('/content/file/delete/', 'Admin\Post\DeleteFileController@post');

//end

//Edit
Route::get('/admin/Edit/{content_id}', 'Admin\Post\EditController@get_news');
Route::post('/admin/Edit/', 'Admin\Post\EditController@post');
//end Edit
//media
Route::get('/admin/media/createMedia', 'media\GalleryController@get');
Route::get('/admin/media/deleteMedia', 'media\GalleryController@get_');
Route::post('/admin/media/createMedia', 'media\GalleryController@post');
//end media


