<?php
/**
 * Created by PhpStorm.
 * User: Shadow
 * Date: 31/08/2017
 * Time: 10:13 AM
 */

namespace App\Http\Controllers\Admin\ChangePass;


use App\driver\SysUsers;
use App\Http\Controllers\Base\Base;
use App\Utils\ValidationForm;
use Illuminate\Http\Request;

class ChangePassController extends Base
{

    function __construct(Request $request)
    {
        $type = Base::get_user_type();
        parent::__construct($request, $type, 'ADMIN');
    }

    public function get(){
        return view('admin.Changepass', compact('type'));
    }
    public function post()
    {
        $password = $this->request->input('password');
        $valid = new ValidationForm();
        $new_pass = $this->request->input('new_pass');
        $conf_pass = $this->request->input('conf_pass');


        if (!$valid->requiredField($password)) {
            $err['E_password'] = 'گذرواژه را وارد کنید';
        } else $err['E_password'] = '';
        if (!$valid->requiredField($new_pass)) {
            $err['E_new_pass'] = 'گذرواژه جدید را وارد کنید';
        } else $err['E_new_pass'] = '';
        if (!$valid->requiredField($conf_pass)) {
            $err['E_conf_pass'] = 'تایید گذرواژه را وارد کنید';
        } else $err['E_conf_pass'] = '';
        if (!$valid->matchPassword($new_pass, $conf_pass)) {
            $err['E_match'] = 'تایید گذرواژه با گذرواژه مطابقت ندارد';
        } else $err['E_match'] = '';

        if ($valid->requiredField($password) and $valid->requiredField($new_pass)
            and $valid->requiredField($conf_pass) and $valid->matchPassword($new_pass, $conf_pass)){
            $user_id = $this->request->session()->get('userid');
            $obj_user = new SysUsers($user_id);

            $user = $obj_user->getOne();
            $hdb = $user['password'];

            if (password_verify($password, $hdb)){
                $new_hash = password_hash($new_pass, PASSWORD_DEFAULT);
                $obj_user = new SysUsers($user_id, null, null, null,
                    null,null, null, $new_hash);
                $obj_user->updateRecord();

                return json_encode([
                    "status" => 'success',
                    "message" => 'گذرواژه با موفقیت تغییر کرد'
                ]);
            }else $err['E_pass_incorrect'] = 'گذرواژه وارد شده اشتباه است';

        }


        return json_encode([
            "status" => 'error',
            "message" => $err
        ]);
    }
}