<?php
/**
 * Created by PhpStorm.
 * User: Shadow
 * Date: 28/07/2017
 * Time: 09:57 AM
 */

namespace App\Http\Controllers\Site\Register;


use App\driver\SysUsers;
use App\Http\Controllers\Base\Base;
use App\Http\Controllers\Controller;
use App\Utils\PasswordHashing;
use App\Utils\ValidationForm;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    private $request;

    function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function get()
    {
        $type = Base::get_user_type();
        return view('site.register',compact('type'));
    }


    public function post()
    {

        $fname_ = $this->request->input('fname');
        $lname_ = $this->request->input('lname');
        $username_ = $this->request->input('username');
        $password_ = $this->request->input('password');
        $confirm_password_ = $this->request->input('conf_pass');




        $valid = new ValidationForm();
        $err = [];
        $err['E_exist_user'] = '';
        if (!$valid->req_alpha_Field($fname_)) {
            $err["E_fname"] = 'نام خود را فارسی وارد کنید';
        } else $err["E_fname"] = '';
        if (!$valid->req_alpha_Field($lname_)) {
            $err["E_lname"] = 'نام خانوادگی خود را فارسی وارد کنید';
        } else $err["E_lname"] = '';
        if (!$valid->requiredField($username_)) {
            $err["E_username"] = 'نام کاربری را وارد کنید';
        } else $err["E_username"] = '';
        if (!$valid->requiredField($password_)) {
            $err["E_pass"] = 'گذرواژه را وارد کنید';
        } else $err["E_pass"] = '';
        if (!$valid->requiredField($confirm_password_)) {
            $err["E_con_pass"] = 'تایید گذرواژه را وارد کنید';
        } else $err["E_con_pass"] = '';
        if (!$valid->matchPassword($password_, $confirm_password_)) {
            $err["E_match"] = 'تایید گذرواژه باید با گذرواژه مطابقت داشته باشد';
        } else $err["E_match"] = '';


        if ($valid->matchPassword($password_, $confirm_password_) and $valid->req_alpha_Field($fname_)
            and $valid->req_alpha_Field($lname_) and $valid->requiredField($username_)
        ) {
            $h_pass = password_hash($password_, PASSWORD_DEFAULT, ['cost' => 15]);
            //print_r(array(null, $username_, $fname_, $lname_, null, null, null, $h_pass, 1));exit;
            $user = new SysUsers(null, $username_, $fname_, $lname_, null, null, null, $h_pass, 1);
            $userid = $user->insert();

            if ($userid != false) {
                $this->request->session()->put('userid', $userid);
                $this->request->session()->put('username', $username_);
                $this->request->session()->put('usergroup', 1);

                return json_encode([
                    "status" => 'success'
                ]);
            }
            else {
                $err['E_exist_user'] = 'این نام کاربری از قبل وجود دارد ';
            }

        }

        return json_encode([
            "status" => 'error',
            "message" => $err

        ]);
    }

}