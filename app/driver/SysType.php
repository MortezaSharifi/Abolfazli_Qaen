<?php

/**
 * Created by PhpStorm.
 * User: salari
 * Date: 7/12/2017
 * Time: 4:31 PM
 */

namespace App\driver;
use App\Type;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Mockery\Exception;



class SysType
{
    private $id;
    private $type;
    private $content_id;


    public function __construct($id = null, $type=null, $content_id=null)
    {

        $this->id = $id;
        $this->type = $type;
        $this->content_id = $content_id;

    }

    public function getAll()
    {
        try {
            return Type::all();
        } catch (Exception $e) {
            return [];
        }

    }

    public function getOne()
    {
        try {
            return Type::all()->where('id', $this->id);
        } catch (Exception $e) {
            return [];
        }

    }

    public function getTypesForContent()
    {
        try {
            $h = \DB::table('type')->select('type')->where('content_id', $this->content_id)->get();
            $list = [];
            for($i=0; $i<count($h); $i++){
                array_push($list, $h[$i]->type);
            };
            return $list;
        } catch (Exception $e) {
            return [];
        }

    }


    public function insert()
    {
        try {
            $type_post= new Type();
            $type_post->type = $this->type;
            $type_post->content_id = $this->content_id;

            $type_post->save();

            return true;
        } catch (Exception $e) {
            return false;
        }

    }

    public function deleteRecord()
    {
        try {
            $type_post = Type::find($this->id);

            $type_post->delete();
            return true;
        } catch (Exception $e) {
            return false;
        }

    }

    public function updateRecord()
    {
        try {
            $type_post = \DB::table('type')->where('content_id',$this->content_id)->update(['type'=>$this->type]);
            return $type_post;
        } catch (Exception $e) {
            return false;
        }

    }
    public function deleteContentType()
    {
        try {
            $_Type = Type::where('content_id', $this->id);

            $_Type->delete();
            return true;
        } catch (QueryException $e) {
            return false;
        }catch (Exception $ee){
            return false;
        }

    }
}