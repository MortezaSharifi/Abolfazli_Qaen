@extends('base.admin_base')
@section('title','گزارشات')
@section('css')
    <link href="{{URL::asset('css/plugins/dataTables.bootstrap.css')}}" rel="stylesheet">
@endsection
@section('report_nav_nav')
    class = "active"
@stop
@section('body')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                            <tr>
                                <th>نام</th>
                                <th>نام خانوادگی</th>
                                <th>مبلغ</th>
                                <th>نوع پرداختی</th>
                                <th>تاریخ</th>
                                <th>توضیحات</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($details as $detail)
                                <tr>
                                    <td>{{$detail->fname}}</td>
                                    <td>{{$detail->lname}}</td>
                                    <td>{{$detail->value}}</td>
                                    <td>{{$detail->type}}</td>
                                    <td>{{$detail->date}}</td>
                                    <td>{{$detail->text}}</td>

                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->

                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="{{url::asset('js/jquery/jquery.dataTables.min.js')}}"></script>
    <script src="{{url::asset('js/bootstrap/dataTables.bootstrap.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('#dataTables-example').dataTable();
        });
    </script>
@endsection