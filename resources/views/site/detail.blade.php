@extends('base.site_base')
@section('title','جزئیات')
@section('content')

    <div class="container">
        <div class="row">
            <!-- start left bar content -->
            <div class=" col-sm-12 col-md-6 col-lg-6">
                <div class="row">
                    <div class="leftbar_content">
                        <h2>مشروح خبر </h2>

                        <div class="singlepost_area">

                            <div class="singlepost_content">
                                <div>
                                    @if($pic == null)
                                        <a href="#"><img class="img-center"
                                                    src="{{asset('img/stuff_img1.jpg')}}" style="width: 400px;height: 300px"
                                                    alt="img"></a>
                                    @else

                                        <a href="#"><img class="img-center"
                                                    src="/content/download/{{$pic}}" style="width: 400px;height: 300px"

                                                    alt="img"></a>
                                    @endif
                                </div>
                                <p>
                                <h3>{{$news->caption}}</h3>
                                <p>{!! $news->text !!}</p>


                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-4 col-lg-offset-1 col-md-offset-1">
                <div class="single_blog_sidebar wow fadeInUp">
                    <h2>تصاویر بیشتر</h2>
                    <ul class="featured_nav">
                        <li>

                            @if($pic == null)
                                <a class="featured_img" href="<?= URL('gallery') ?> ">
                                    <img src="{{asset('img/stuff_img1.jpg')}}" alt="img" style="width: 100%;"> <h6>برای مشاهده تصاویر بیشتر اینجا کلیک کنید</h6>

                                </a>
                            @else
                                <a class="featured_img" href="<?= URL('gallery') ?> ">
                                    <img src="/content/download/{{$pic}}" alt="img" style="width: 100%;"><h6>برای مشاهده تصاویر بیشتر اینجا کلیک کنید</h6>

                                </a>
                            @endif

                        </li>
                    </ul>
                </div>
                </div>
            </div>
        </div>

@endsection