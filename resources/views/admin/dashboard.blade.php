@extends('base.admin_base')
@section('title','داشبورد')
@section('dash_nav')
    class = "active"
@stop
@section('body')
    <div class="row">
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-dark">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-money fa-4x"></i>
                        </div>
                        <div class="col-xs-9 text-left">
                            <div class="huge">{{$cost_w->value}}</div>
                            <div>هزینه ترحیم</div>
                        </div>

                    </div>
                </div>
                <a href="<?= url('admin/cost') ?>">
                    <div class="panel-footer">
                        <span class="pull-right">برای تغییر کلیک کنید</span>
                        <span class="pull-left"><i class="fa fa-arrow-circle-left"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-money fa-4x"></i>
                        </div>
                        <div class="col-xs-9 text-left">
                            <div class="huge">{{$cost_c->value}}</div>
                            <div>هزینه جشن</div>
                        </div>

                    </div>
                </div>
                <a href="<?= url('admin/cost') ?>">
                    <div class="panel-footer">
                        <span class="pull-right">برای تغییر کلیک کنید</span>
                        <span class="pull-left"><i class="fa fa-arrow-circle-left"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-users fa-4x"></i>
                        </div>
                        <div class="col-xs-9 text-left">
                            <div class="huge">{{$count}}</div>
                            <div>تعداد اعضا</div>
                        </div>

                    </div>
                </div>
                <a href="<?= url('admin/users') ?>">
                    <div class="panel-footer">
                        <span class="pull-right">جزئیات بیشتر</span>
                        <span class="pull-left"><i class="fa fa-arrow-circle-left"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-red">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-bank fa-3x"></i>
                        </div>
                        <div class="col-xs-9 text-left">
                            <div class="huge">{{$total}}</div>
                            <div>مجموع مبالغ اهدا شده</div>
                        </div>

                    </div>
                </div>
                <a href="<?= url('admin/report') ?>">
                    <div class="panel-footer">
                        <span class="pull-right">مشاهده گزارشات</span>
                        <span class="pull-left"><i class="fa fa-arrow-circle-left"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> آخرین پرداخت ها
                    <div class="pull-left">
                        <a href="<?= url('admin/report') ?>" class="btn btn-xs btn-default" style="color: black">مشاهده بیشتر</a>
                    </div>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped">
                                    <thead>
                                    <tr>
                                        <th>نام</th>
                                        <th>نام خانوادگی</th>
                                        <th>نوع پرداختی</th>
                                        <th>مبلغ</th>
                                        <th>تاریخ</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($last_pay as $pay)
                                        <tr>
                                            <td>{{$pay->fname}}</td>
                                            <td>{{$pay->lname}}</td>
                                            <td>{{$pay->type}}</td>
                                            <td>{{$pay->value}}</td>
                                            <td>{{$pay->date}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.col-lg-4 (nested) -->
                        <div class="col-lg-8">
                            <div id="morris-bar-chart"></div>
                        </div>
                        <!-- /.col-lg-8 (nested) -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
    </div>
@stop