CKEDITOR.editorConfig = function( config ) {
	config.language = 'fa';
	config.uiColor = '#428bcd';
	config.height = 300;
	config.toolbarCanCollapse = true;
};