<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}"/>
    <title>@yield('title')</title>

    <!-- Bootstrap -->
    <link href="{{ URL::asset('css/bootstrap-3.3.7.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/bootstrap-rtl.css') }}" rel="stylesheet">
    <link href="{{URL::asset('css/common.css')}}" rel="stylesheet">

    <!-- for fontawesome icon css file -->
    <link href="{{ URL::asset('css/font-awesome.min.css') }}" rel="stylesheet">

    <!-- for content animate css file -->
    <link rel="stylesheet" href="{{ URL::asset('css/animate.css') }}">
    <!-- main site css file -->
    <link href="{{ URL::asset('css/structure.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{URL::asset('plugin/toastr/toastr.css')}}">

@yield('css')

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="{{ URL::asset('https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js') }}"></script>
    <script src="{{ URL::asset('https://oss.maxcdn.com/respond/1.4.2/respond.min.js') }}"></script>
    <![endif]-->
</head>
<body>
<!-- Preloader -->

<div id="preloader">
    <div id="status">&nbsp;</div>
</div>
<!--End Preloader -->


<a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
<!-- ==================start header=============== -->
<header id="header">
    <div class="container">
        <!-- Static navbar -->
        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                            aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?= Url('/index/1') ?>"><span>هیئت</span>ابوالفضلی</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav custom_nav">
                        <li><a href="<?= Url('/index/1') ?>">صفحه نخست</a></li>
                        <li><a href="#">تاریخچه</a></li>
                        <li><a href="<?= Url('gallery') ?>">گالری</a></li>
                        <li><a href="<?= Url('payment') ?>">پرداخت</a></li>
                        @if($type == 'MEMBER')
                            <li><a href="<?= Url('profile') ?>">پروفایل</a></li>
                        @endif
                    </ul>

                </div><!--/.nav-collapse -->

            </div><!--/.container-fluid -->

        </nav>
        @if ($type == 'MEMBER')

            <ul class="nav navbar-nav custom_nav pull-left" id="pro">
                <li class="dropdown margin-left-5">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                       aria-expanded="false">
                        <span class="glyphicon glyphicon-user logout"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="<?= Url('profile')?>"><span class="glyphicon glyphicon-user padding-left-3"></span>
                                <?php
                                echo session('username');
                                ?>
                            </a>
                        </li>


                        <li>
                            <a href="<?= Url('profile')?>"><span class="glyphicon glyphicon-edit padding-left-3"></span>
                                ویرایش پروفایل
                            </a>
                        </li>
                        <li>
                            <a href="#" id="logout"><span class="glyphicon glyphicon-log-out padding-left-3"></span>
                                خروج
                            </a>
                        </li>

                    </ul>
                </li>
            </ul>
        @endif
        @if($type == null)
            <ul id="log-reg" class="log-reg">
                <!--=========================================Login=========================================-->
                <li><a href="<?= Url('login') ?>" data-toggle="modal"
                       data-target="#login-modal">ورود</a></li>

                <!--=========================================EndLogin========================================-->
                <span style="margin:0 5px">|</span>
                <li><a href="<?= Url('register') ?>">ثبت نام</a></li>

            </ul>
        @endif
    </div>
</header>
<!-- ==================End header=============== -->
<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="loginmodal-container">
            <h1 class="colorWhite">ورود</h1>
            <hr class="width-50-darsad">

            <form>
                <input type="text" id="user" placeholder="نام کاربری">
                <input type="password" id="pass" placeholder="گذرواژه">
                <button type="button" id="login"
                        class="login loginmodal-submit width-100-darsad">ورود
                </button>
            </form>

        </div>
    </div>
</div>
<section id="contentbody">

    @yield('content')
</section>
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="footer_inner">
                    <p class="pull-left margin-top-10">کلیه حقوق برای این<a class="margin-3-side"
                                                                            href="<?= Url('/') ?>">سایت</a>محفوض است
                    </p>
                    <div class="pull-right colorWhite">
                        <h4 class="colorWhite">هیئت ابوالفضلی قاین</h4>
                        <i class="fa fa-map-marker margin-3-side"></i><span> خراسان جنوبی-قاین-میدان امام خمینی-هیئت ابوالفضلی</span>
                        <p><i class="fa fa-phone-square margin-3-side"></i>تلفن تماس:</p>
                        <p><i class="fa fa-envelope-o margin-3-side"></i>ایمیل:</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{ URL::asset('js/jquery-1.11.0.js') }}"></script>
{{--<script src="{{ URL::asset('js/jquery-1.11.0.js') }}"></script>--}}
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
<!-- For content animatin  -->
<script src="{{ URL::asset('js/wow.min.js') }}"></script>
<!-- custom js file include -->
<script src="{{ URL::asset('js/custom.js') }}"></script>
<script src="{{URL::asset('plugin/toastr/toastr.js')}}"></script>
<script src="{{URL::asset('js/func_toastr.js')}}"></script>
<script>

    $(document).on('click', '#login', function () {

        var username = $('#user').val();
        var password = $('#pass').val();
        var _token = $('meta[name="_token"]').attr('content');

        $.ajax({
            type: 'POST',
            url: "{{action('Site\LoginController@post')}}",
            data: {

                "username": username,
                "password": password,
                "_token": _token
            },

            success: function (_ret) {
                var ret = JSON.parse(_ret);
                console.log(ret);
                if (ret['status'] === 'success') {
                    if (ret['group'] === 2) {
                        window.location = '{{  action('Admin\Dashboard\DashboardController@get') }}';
                    }
                    if (ret['group'] === 1) {
                        location.reload();
                    }


                }
                if (ret['status'] === 'error') {
                    _toastr(ret['message']['E_username'], "top-right", "error", false);
                    _toastr(ret['message']['E_password'], "top-right", "error", false);
                    _toastr(ret['message']['E_verify'], "top-right", "error", false);
                }
                console.log(ret);

            },
            error: function (e) {
                console.log(e);
            }
        })

    });
    $(document).on('click', '#logout', function () {

        var _token = $('meta[name="_token"]').attr('content');

        $.ajax({
            type: 'POST',
            url: "{{action('Site\LoginController@logout')}}",
            data: {
                "_token": _token
            },

            success: function (_ret) {
                var ret = JSON.parse(_ret);
                if (ret['status'] === 'success') {
                    location.reload();
                }

                console.log(ret);

            },
            error: function (e) {
                console.log(e);
            }
        })

    })
</script>

@yield('js')
</body>
</html>