<?php

namespace App\Http\Controllers\Costhaspayment;

use App\driver\SysCosthaspayment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

class CosthaspaymentController extends Controller
{
    public function get()
    {

        $object_cost_payment = new SysCosthaspayment();
        $all_cost_payment = $object_cost_payment->getAll();
        $cost_payment1 = $object_cost_payment->getOne();
        return view('test',compact('all_cost_payment'));
    }

    public function post(Request $request)
    {
        // get data from client request
        $f = $request->input('f');

        $objectCosthaspayment= new SysCosthaspayment(null,4,15,$f);
        $objectCosthaspayment->insert();

        $objectCosthaspayment= new SysCosthaspayment(5);
        $objectCosthaspayment->deleteRecord();
        return $f;
    }


}