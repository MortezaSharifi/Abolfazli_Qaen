<?php
/**
 * Created by PhpStorm.
 * User: Shadow
 * Date: 31/08/2017
 * Time: 03:48 PM
 */

namespace App\Http\Controllers\Admin\Messages;


use App\driver\SysUsers;
use App\Http\Controllers\Base\Base;
use App\Utils\ValidationForm;
use Illuminate\Http\Request;

class SmsController extends Base
{
    function __construct(Request $request)
    {
        $type = Base::get_user_type();
        parent::__construct($request, $type, 'ADMIN');
    }

    public function get(){
        return view('admin.Sms', compact('type'));
    }

    public function post(){
        $message = $this->request->input('message');
        $message = trim($message);

        $valid = new ValidationForm;
        if (!$valid->requiredField($message)){
            $err['E_message'] = 'متنی وارد نشده است';
        }else $err['E_message'] = '';

        if ($valid->requiredField($message)){
            $obj_user = new SysUsers();
            $phone = $obj_user->get_number();
            return json_encode([
                'status' => 'success',
                'message' =>
                    'پیام ارسال شد'
            ]);
        }
        return json_encode([
            'status' => 'error',
            'message' => $err
        ]);
    }
}