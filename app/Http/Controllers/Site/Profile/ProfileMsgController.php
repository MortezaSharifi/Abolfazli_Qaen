<?php
/**
 * Created by PhpStorm.
 * User: Shadow
 * Date: 15/08/2017
 * Time: 11:32 AM
 */

namespace App\Http\Controllers\Site\Profile;


use App\driver\SysComment;
use App\Http\Controllers\Base\Base;
use App\Utils\ValidationForm;


class ProfileMsgController extends Base
{

    public function post(){
        $message = $this->request->input('message');
        $message = trim($message);

        $valid = new ValidationForm;
        if (!$valid->requiredField($message)){
            $err['E_message'] = 'متنی وارد نشده است';
        }else $err['E_message'] = '';

        if ($valid->requiredField($message)){
            $userid = $this->request->session()->get('userid');
            $comment = new SysComment(null, $message,null, $userid, 'F');
            $comment->insert();
            return json_encode([
                'status' => 'success',
                'message' => 'پیام ارسال شد'
            ]);
        }
        return json_encode([
            'status' => 'error',
            'message' => $err
        ]);
    }
}