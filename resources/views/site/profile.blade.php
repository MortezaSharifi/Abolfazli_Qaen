@extends('base.site_base')
@section('title','پروفایل')
@section('css')
    <link rel="stylesheet" href="{{URL::asset('plugin/toastr/toastr.css')}}">
@endsection
@section('content')
    <div class="container">

        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <!-- Nav tabs -->
                    <div class="card">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="#edit-profile" aria-controls="profile"
                                                                      role="tab" data-toggle="tab">ویرایش مشخصات</a>
                            </li>
                            <li role="presentation" class="active"><a href="#messages" aria-controls="messages" role="tab"
                                                       data-toggle="tab">پیام ها</a></li>
                            <li role="presentation"><a href="#pay_report" aria-controls="settings" role="tab"
                                                       data-toggle="tab">گزارش پرداخت ها</a></li>
                            <li role="presentation"><a href="#change-pass" aria-controls="settings" role="tab"
                                                       data-toggle="tab">تغییر رمز عبور</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane" id="edit-profile">

                                <form class="form-horizontal" method="post" action="#">

                                    <div class="form-group">
                                        <label for="name" class="cols-sm-2 control-label color333333">نام</label>
                                        <div class="cols-sm-10">
                                            <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-user fa"
                                                                                       aria-hidden="true"></i></span>
                                                <input type="text" class="form-control" name="fname" id="fname"
                                                       placeholder="نام خود را وارد کنید"
                                                       value="{{$user_info->fname}}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="lname" class="cols-sm-2 control-label color333333">نام
                                            خانوادگی</label>
                                        <div class="cols-sm-10">
                                            <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-user fa"
                                                                                       aria-hidden="true"></i></span>
                                                <input type="text" class="form-control" name="lname" id="lname"
                                                       placeholder="نام خانوادگی خود را وارد کنید"
                                                       value="{{$user_info->lname}}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="username" class="cols-sm-2 control-label color333333">نام
                                            کاربری</label>
                                        <div class="cols-sm-10">
                                            <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-users fa"
                                                                                       aria-hidden="true"></i></span>
                                                <input type="text" class="form-control" name="username"
                                                       id="username" placeholder="نام کاربری خود را وارد کنید"
                                                       value="{{$user_info->username}}"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="phone" class="cols-sm-2 control-label color333333">شماره
                                            همراه</label>
                                        <div class="cols-sm-10">
                                            <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-mobile fa"
                                                                                       aria-hidden="true"></i></span>
                                                <input type="text" class="form-control" name="phone"
                                                       id="phone" placeholder="شماره همراه خود را وارد کنید"
                                                       value="{{$user_info->phone}}"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="email" class="cols-sm-2 control-label color333333">
                                            ایمیل</label>
                                        <div class="cols-sm-10">
                                            <div class="input-group">
                                                <span class="input-group-addon">@</span>
                                                <input type="email" class="form-control" name="email"
                                                       id="email" placeholder="ایمیل خود را وارد کنید"
                                                       value="{{$user_info->email}}"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="address" class="cols-sm-2 control-label color333333">آدرس</label>
                                        <div class="cols-sm-10">

                                                <textarea class="form-control" name="address" id="address">
{{$user_info->address}}
                                                </textarea>

                                        </div>
                                    </div>


                                    <div class="form-group ">
                                        <button type="button" id="edit"
                                                class="btn red_btn btn-lg btn-block login-button center-block width-40-darsad">
                                            ویرایش
                                        </button>
                                    </div>

                                </form>

                            </div>
                            <div role="tabpanel" class="tab-pane active" id="messages">
                                <form action="#" method="post">
                                    <div class="form-group">
                                        <label for="message" class="cols-sm-2 control-label color333333">نظرات و
                                            پیشنهادات</label>
                                        <div class="cols-sm-10">

                                            <textarea class="form-control" id="message"></textarea>

                                        </div>
                                    </div>


                                    <div class="form-group ">
                                        <button type="button" id="send_msg"
                                                class="btn red_btn btn-lg btn-block center-block width-30-darsad">
                                            ارسال
                                        </button>
                                    </div>
                                </form>
                                <hr class="width-100-darsad">

                                <div class="row padding-5" style="height: 150px;overflow: scroll">
                                    @foreach( $msgs as $msg)
                                        @if($msg->status == 'F')
                                            <div class="col-lg-12">
                                                <div id="tb-testimonial" class="testimonial testimonial-primary">
                                                    <div class="testimonial-desc">

                                                        <div class="testimonial-writer">
                                                            <div class="testimonial-writer-name">{{$msg->username}}</div>
                                                            <div class="testimonial-writer-designation">
                                                                {{$msg->date}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="testimonial-section">
                                                        {{$msg->text}}
                                                    </div>

                                                </div>
                                            </div>
                                        @endif


                                        @if($msg->status == 'T')
                                            <div class="col-lg-12">
                                                <div id="tb-testimonial" class="testimonial testimonial-success">
                                                    <div class="testimonial-desc">

                                                        <div class="testimonial-writer">
                                                            <div class="testimonial-writer-name">ادمین</div>
                                                            <div class="testimonial-writer-designation">
                                                                {{$msg->date}}

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="testimonial-section">
                                                        {{$msg->text}}
                                                    </div>

                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>

                            </div>

                            <div role="tabpanel" class="tab-pane" id="pay_report">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                        <thead>
                                        <tr>
                                            <th>نوع پرداختی</th>
                                            <th>مبلغ</th>
                                            <th>تاریخ</th>
                                            <th>توضیحات</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($details as $detail)
                                            <tr>
                                                <td>{{$detail->type}}</td>
                                                <td>{{$detail->value}}</td>
                                                <td>{{$detail->date}}</td>
                                                <td>{{$detail->text}}</td>

                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>

                            <div role="tabpanel" class="tab-pane" id="change-pass">
                                <form class="form-horizontal" method="post" action="#">
                                    <div class="form-group">
                                        <label for="password" class="cols-sm-2 control-label color333333">گذرواژه
                                            قبلی</label>
                                        <div class="cols-sm-10">
                                            <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-lock fa-lg"
                                                                                       aria-hidden="true"></i></span>
                                                <input type="password" class="form-control" name="password"
                                                       id="password" placeholder="گذرواژه قبلی خود را وارد کنید"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="new_pass" class="cols-sm-2 control-label color333333">گذرواژه
                                            جدید</label>
                                        <div class="cols-sm-10">
                                            <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-lock fa-lg"
                                                                                       aria-hidden="true"></i></span>
                                                <input type="password" class="form-control" name="new_pass"
                                                       id="new_pass" placeholder=" گذرواژه جدید خود را وارد کنید"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="conf_pass" class="cols-sm-2 control-label color333333">تکرار گذرواژه
                                            جدید</label>
                                        <div class="cols-sm-10">
                                            <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-lock fa-lg"
                                                                                       aria-hidden="true"></i></span>
                                                <input type="password" class="form-control" name="conf_pass"
                                                       id="conf_pass"
                                                       placeholder=" گذرواژه جدید خود را مجددا وارد کنید"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <button type="button" id="change_pass"
                                                class="margin-top-10 btn red_btn btn-lg btn-block center-block width-40-darsad">
                                            تغییر گذرواژه
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{url::asset('js/jquery/jquery.dataTables.min.js')}}"></script>
    <script src="{{url::asset('js/bootstrap/dataTables.bootstrap.min.js')}}"></script>
    <script>

        $(document).ready(function() {
            $('#dataTables-example').dataTable();
        });

        $(document).on('click', '#edit', function () {

            var fname = $('#fname').val();
            var lname = $('#lname').val();
            var username = $('#username').val();
            var phone = $('#phone').val();
            var email = $('#email').val();
            var address = $('#address').val();
            var _token = $('meta[name="_token"]').attr('content');

            $.ajax({
                type: 'POST',
                url: "{{action('Site\Profile\ProfileController@post')}}",
                data: {
                    "fname": fname,
                    "lname": lname,
                    "username": username,
                    "phone": phone,
                    "email": email,
                    "address": address,
                    "_token": _token
                },


                success: function (_ret) {
                    var ret = JSON.parse(_ret);


                    if (ret['status'] === 'error') {
                        _toastr(ret['message']['E_fname'], "top-right", "error", false);
                        _toastr(ret['message']['E_lname'], "top-right", "error", false);
                        _toastr(ret['message']['E_username'], "top-right", "error", false);
                        _toastr(ret['message']['E_exist_user'], "top-right", "error", false);
                        _toastr(ret['message']['E_email'], "top-right", "error", false);


                    }
                    console.log(ret);
                    if (ret['status'] === 'success') {
                        _toastr(ret['message'], "top-left", "success", false);


                    }
                },
                error: function (e) {
                    console.log(e);
                }
            })

        });
        $(document).on('click', '#send_msg', function () {


            var message = $('#message').val();
            var _token = $('meta[name="_token"]').attr('content');

            $.ajax({
                type: 'POST',
                url: "{{action('Site\Profile\ProfileMsgController@post')}}",
                data: {

                    "message": message,
                    "_token": _token
                },


                success: function (_ret) {
                    var ret = JSON.parse(_ret);


                    if (ret['status'] === 'error') {

                        _toastr(ret['message']['E_message'], "top-right", "error", false);


                    }
                    console.log(ret);
                    if (ret['status'] === 'success') {
//                        _toastr(ret['message'], "top-left", "success", false);
                        $('#message').val('');
                        location.reload();
                    }
                },
                error: function (e) {
                    console.log(e);
                }
            })

        });
        $(document).on('click', '#change_pass', function () {

            var password = $('#password').val();
            var new_pass = $('#new_pass').val();
            var conf_pass = $('#conf_pass').val();
            var _token = $('meta[name="_token"]').attr('content');

            $.ajax({
                type: 'POST',
                url: "{{action('Site\Profile\ProfileController@changePass')}}",
                data: {

                    "password": password,
                    "new_pass": new_pass,
                    "conf_pass": conf_pass,
                    "_token": _token
                },


                success: function (_ret) {
                    var ret = JSON.parse(_ret);


                    if (ret['status'] === 'error') {
                        $('input[type = "password"]').val('');
                        _toastr(ret['message']['E_password'], "top-right", "error", false);
                        _toastr(ret['message']['E_new_pass'], "top-right", "error", false);
                        _toastr(ret['message']['E_conf_pass'], "top-right", "error", false);
                        _toastr(ret['message']['E_match'], "top-right", "error", false);


                    }
                    console.log(ret);
                    if (ret['status'] === 'success') {
                        _toastr(ret['message'], "top-left", "success", false);
                        $('input[type = "password"]').val('');


                    }
                },
                error: function (e) {
                    console.log(e);
                }
            })

        })
    </script>
@endsection