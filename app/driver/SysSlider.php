<?php
/**
 * Created by PhpStorm.
 * User: Shadow
 * Date: 25/07/2017
 * Time: 02:24 PM
 */
namespace App\driver;
use App\Slider;
use Illuminate\Database\Eloquent\Model;
use Mockery\Exception;

class SysSlider
{
    private $id;
    private $name;
    private $multimedia_id;

    public function __construct($id = null, $name = null, $multimedia_id = null)
    {

        $this->id = $id;
        $this->name = $name;
        $this->content_id = $multimedia_id;

    }

    public function getAll()
    {
        try {
            return Slider::all();
        } catch (Exception $e) {
            return [];
        }

    }

    public function getOne()
    {
        try {
            return Slider::all()->where('id', $this->id);
        } catch (Exception $e) {
            return [];
        }

    }

    public function insert()
    {
        try {
            $_Slider = new Slider;
            $_Slider->name = $this->name;
            $_Slider->multimedia_id = $this->multimedia_id;

            $_Slider->save();

            return true;
        } catch (Exception $e) {
            return false;
        }

    }

    public function deleteRecord()
    {
        try {
            $_Slider = Slider::find($this->id);

            $_Slider->delete();
            return true;
        } catch (Exception $e) {
            return false;
        }

    }

    public function updateRecord()
    {
        try {
            $_Slider = Slider::find($this->id);

            if (isset($this->name)) {
                $_Slider->name = $this->name;
            }
            if (isset($this->content_id)) {
                $_Slider->multimedia_id = $this->multimedia_id;
            }


            $_Slider->save();


            return true;
        } catch (Exception $e) {
            return false;
        }

    }
}