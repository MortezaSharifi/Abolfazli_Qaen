<?php

namespace App\Http\Controllers\Admin\Post;


use App\driver\SysMultimedia;
use App\driver\SysType;
use App\Http\Controllers\Base\Base;
use App\Http\Controllers\Controller;
use App\driver\SysContent;
use App\Multimedia;
use App\Utils\PasswordHashing;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Utils\Upload;
use App\Utils\MimeType;
use \View as View;
class ContentController extends Base
{
    function __construct(Request $request)
    {
        $type = Base::get_user_type();
        parent::__construct($request, $type,'ADMIN');
    }

    public function get()
    {
        return view('admin.content');

    }

    public function delete()
    {
        $news_id = (int)$this->request->input('news_id');
        $sysType = new SysType($id = $news_id);

        $deleteRowType = $sysType->deleteContentType();
        if ($deleteRowType === false) {
            return json_encode([
                "status"=>'error',
                "message"=>'اطلاعات حذف نشد مجددا تلاش کنید'
            ]);
        }

        $sysMultimedia = new SysMultimedia($id = $news_id);
        $deleteRowMm = $sysMultimedia->deleteContentRecord();

        if ($deleteRowMm === false) {
            return json_encode([
                "status"=>'error',
                "message"=>'اطلاعات حذف نشد مجددا تلاش کنید'
            ]);

        }
        $sysContent = new SysContent($id = $news_id);
        $deleteRow = $sysContent->deleteRecord();
        if ($deleteRow === false){
            return json_encode([
                "status"=>'error',
                "message"=>'اطلاعات حذف نشد مجددا تلاش کنید'
            ]);

        }

        return json_encode([
            "status"=>'success',
            "message"=>'اطلاعات با موفقیت حذف شد!'
        ]);
    }

    public function post()
    {
        // get data from client request
        $title = $this->request->input('caption');
        $news = $this->request->input('news');
        $notification = $this->request->input('notification');
        $gallery = $this->request->input('gallery');
        $text = $this->request->input('text');
        $files = json_decode($this->request->input('files'),true);
        $user_id = $this->request->session()->get('userid');

        $objectContent = new SysContent(null, $user_id , $title, $text ,null );
//        $objectContent = new SysContent(null,2 ,$title,$text ,null );
        $content_id = $objectContent->insert();

        for($i=0; $i < count($files); $i++){
            $contentType = $files[$i]["contentType"];
            $fileName = $files[$i]["fileName"];
            $fileId = $files[$i]["fileId"];
            $objectMultiMedia = new SysMultimedia(null, $fileName, $content_id, $contentType, $fileId);
            $objectMultiMedia->insert();
        }

        if ($news == "news"){
            $objectType = new SysType(null,'news',$content_id);
            $objectType->insert();
        }
        if ($notification == "notification"){
            $objectType = new SysType(null,'notification',$content_id);
            $objectType->insert();

        }
        if($gallery == "gallery"){
            $objectType = new SysType(null,'gallery',$content_id);
            $objectType->insert();

        }



        return json_encode([
            "status"=>'success',
            "message"=>'اطلاعات با موفقیت ثبت شد!'
        ]);
    }


}