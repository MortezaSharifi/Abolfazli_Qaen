@extends('base.site_base')
@section('title','گالری تصاویر')
@section('content')
    <div id="filters" class="container margin-top-0">
        <label for="show_img" class="btn btn-default margin-10">تصاویر <span class="badge">&check;</span></label>
    </div>
    <div class="container">
        <div class="row margin-bottom-20">
            @foreach($listGallery as $g)
                @if(explode("/", $g['file_type'])[0] == "image")

                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="row">
                            <!-- start featured post -->
                            <div class="single_blog_sidebar wow fadeInUp">
                                <ul class="featured_nav">
                                    <li>
                                        <a class="featured_img" href="#"><img
                                                    src="/content/download/{{$g['file_id']}}"
                                                    alt="img"></a>
                                        <div class="featured_title">
                                            <p>تصاویر عمرانی </p>
                                        </div>
                                    </li>
                                </ul>
                                <!-- End featured post -->

                            </div>
                        </div>
                    </div>
                @endif
            @endforeach

        </div>
        <div class="clear-both"></div>

        <div id="filters" class="container  margin-top-0">
            <label for="show_vid" class="btn btn-default margin-10">فیلم ها<span class="badge">&check;</span></label>
        </div>

        <div class="row  margin-bottom-30">
            @foreach($listGallery as $g)
                @if(explode("/", $g['file_type'])[0] == "video")
                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="row">
                            <!-- start featured post -->
                            <div class="single_blog_sidebar wow fadeInUp">
                                <ul class="featured_nav">
                                    <li>
                                        <a class="featured_img" href="<?= URL('gallery/img/show') ?>"><img
                                                    src="{{URL::asset('img/featured_img1.jpg')}}" alt="img"></a>
                                        <div class="featured_title">
                                            <p>سایر فیلم ها</p>
                                        </div>
                                    </li>
                                </ul>
                                <!-- End featured post -->

                            </div>
                        </div>
                    </div>
                @endif
            @endforeach

        </div>

        <div id="filters" class="container  margin-top-0">
            <label for="show_adu" class="btn btn-default margin-10">صوت ها<span class="badge">&check;</span></label>

        </div>

        <div class="row">
            @foreach($listGallery as $g)
                @if(explode("/", $g['file_type'])[0] == "audio")
                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="row">
                            <!-- start featured post -->
                            <div class="single_blog_sidebar wow fadeInUp">
                                <ul class="featured_nav">
                                    <li>
                                        <a class="featured_img" href="#"><img
                                                    src="{{URL::asset('img/featured_img1.jpg')}}"
                                                    alt="img"></a>
                                        <div class="featured_title">
                                            <p>مولودی</p>
                                        </div>
                                    </li>
                                </ul>
                                <!-- End featured post -->

                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>

    </div>

@endsection




