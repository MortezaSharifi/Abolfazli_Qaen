<?php
/**
 * Created by PhpStorm.
 * User: Shadow
 * Date: 12/08/2017
 * Time: 07:35 AM
 */

namespace App\Http\Controllers\Site;

use App\driver\SysUsers;
use App\Http\Controllers\Controller;
use App\Utils\ValidationForm;
use Illuminate\Http\Request;


class LoginController extends Controller
{
    private $request;

    function __construct(Request $request)
    {
        $this->request = $request;
    }


    public function post()
    {

        $username_ = $this->request->input('username');
        $password = $this->request->input('password');
        $valid = new ValidationForm();

        if (!$valid->requiredField($username_)) {
            $err['E_username'] = 'نام کاربری را وارد کنید';
        } else $err['E_username'] = '';
        if (!$valid->requiredField($password)) {
            $err['E_password'] = 'گذرواژه را وارد کنید';
        } else $err['E_password'] = '';
        $err['E_verify'] = '';
        if ($valid->requiredField($username_) and $valid->requiredField($password)) {
            $obj_user = new SysUsers(null, $username = $username_);

            $user = $obj_user->get_by_uname();


            $hdb = $user['password'];
            if (password_verify($password, $hdb)) {
                $this->request->session()->put('userid', $user['id']);
                $this->request->session()->put('username', $username_);
                $this->request->session()->put('usergroup', $user['user_group_id']);
                return json_encode([
                    "status" => 'success',
                    "group" => $user['user_group_id']
                ]);

            } else $err['E_verify'] = 'گذرواژه یا نام کاربری درست وارد نشده است';

        }


        return json_encode([
            "status" => 'error',
            "message" => $err
        ]);
    }

    public function logout()
    {
        $this->request->session()->flush();
        return json_encode([
            "status" => 'success'
        ]);
    }

}