<?php

namespace App\Http\Controllers\Site;

use App\driver\pagination;
use App\driver\SysUsers;
use App\Http\Controllers\Base\Base;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\driver\SysContent;
use App\driver\SysMultimedia;
use App\Libraries;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller
{

    public function get($page_num)
    {
        $sysContent = new SysContent();
        $listNotification = $sysContent->getLimitNotification();

        $multimedia = new SysMultimedia();
        $_file_id = $multimedia->get_file_id();

        include(app_path() . '\Libraries\jdf.php');

        $limit = 5;
        $obj_pagination = new pagination("content", $page_num, $limit);
        $class = $obj_pagination->paginate('date');
        if ($class['status'] == '350'){
            foreach ($class['data'] as $one_content) {

                $fileId = $multimedia->get_pic_by_id($one_content->content_id);
                if ($fileId['status'] == '350') {
                    $one_content->file = $fileId['pic'];
                } else if ($fileId['status'] == '300') {
                    $one_content->file = null;
                }

            }
            foreach ($class['data'] as $one_data) {
                $one_data->date = Libraries\jdate("j F Y", strtotime($one_data->date));
            }
        }else if ($class['status'] == '300'){
            $class['data'] = false;
            $class['count'] = 0;
        }


        $type = Base::get_user_type();

        return \View::make('site.home', array(
            'all_page' => $class['count'], 'page_now' => $page_num, 'news' => $class['data'], 'type'=>$type,
            'listNotification' => $listNotification, '_file_id' => $_file_id
        ));

    }

    public function getAllContent()
    {
        $sysContent = new SysContent();
        $allContent = $sysContent->get_All_();

        include(app_path() . '\Libraries\jdf.php');
        foreach ($allContent as $_data) {
            $_data->date = Libraries\jdate("j F Y", strtotime($_data->date));
        }

        return view('admin.ShowContent', compact('allContent'));
    }


    public function getNews($content_id)
    {
        $sysContent = new SysContent($content_id);
        $_news = $sysContent->getOne();
        $sysMultimedia= new SysMultimedia();
        $pic =$sysMultimedia->get_pic($content_id);
        if ($pic['status'] == '350'){
            $picture = $pic['pic'];
        }else if($pic['status'] == '300'){
            $picture = null;
        }
        if ($_news != false) {
           return \View::make('site.detail', array(
                'type' => null, 'news'=>$_news , 'pic'=>$picture
           ));
            } else {
                return redirect()->to('/index/1');
        }
    }
}

