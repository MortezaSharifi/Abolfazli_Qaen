<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Costhaspayment extends Model
{
    protected $table='cost_has_payment';
    public $timestamps = false;
    protected $fillable = [
        'type_name',
    ];
}
