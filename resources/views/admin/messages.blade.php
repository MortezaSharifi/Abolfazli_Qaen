@extends('base.admin_base')
@section('title','پیام ها')
@section('message_nav')
    class = "active"
@endsection
@section('body')
    <div class="row">
        <div class="col-lg-offset-1 col-lg-10">
            <div class="panel panel-info" style="padding: 25px">
                @foreach($msgs as $msg)
                    <div data-id="{{ $msg['id']  }}" class="panel panel-default">
                        <div class="panel-body padding-15">
                            <h4>{{$msg->username}}</h4>
                            <div style="margin: 5px 10px">

                                <strong>{{$msg->date}}</strong>
                                <div style="padding: 10px" class="text-justify">
                                    {{$msg->text}}
                                </div>

                                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12 form-horizontal">
                                    <label class="col-lg-12 col-md-12 col-sm-12 col-xs-12" for="reply">پاسخ
                                        <input id="{{$msg->uid}}" class="uid" hidden>
                                        <textarea id="reply" class="form-control"></textarea>
                                    </label>
                                </div>
                                <div class="col-lg-1 margin-top-10">
                                    <button id="{{$msg->id}}" class="btn btn-sm btn-success send"
                                            style="margin: 10px 0">پاسخ
                                    </button>
                                    <button id="{{$msg->id}}" class="btn btn-sm btn-danger delete" data-toggle="modal"
                                            data-target="#myModal">حذف
                                    </button>
                                </div>

                            </div>

                        </div>
                    </div>
                @endforeach

            </div>
            <!-- Modal -->
            <div class="modal fade closeModal" id="myModal" role="dialog">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content ">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">حذف</h4>
                        </div>
                        <div class="modal-body">
                            <p>آیا از حذف اطمینان دارید؟</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal"><span
                                        class="fa fa-mail-forward"></span>لغو
                            </button>
                            <button type="button" id="delete" class="btn btn-danger"><span class="fa fa-trash"></span>حذف
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        var _token = $('meta[name="_token"]').attr('content');
        var comment_id_ = null;
        $(document).on('click', '.send', function () {
            var reply = $('#reply').val();
            var comment_id_ = $(this).attr('id');
            var uid = $('.uid').attr('id');
            if (comment_id_ !== null) {
                $.ajax({
                    url: '{{ action('Admin\Messages\UserMessage@post') }}',
                    type: 'POST',
                    data: {
                        "reply": reply,
                        "comment_id": comment_id_,
                        "user_id": uid,
                        "_token": _token
                    },
                    success: function (res__) {
                        var res = JSON.parse(res__);
                        if (res['status'] === 'success') {
                            $('div[data-id="' + comment_id_.toString() + '"]').remove();
                            _toastr(res['message'], "top-right", "success", false);
                        }
                        if (res['status'] === 'error') {
                            _toastr(res['message'], "top-right", "error", false);

                        }
                    }
                })
            }

        });
        // when click on the delete button over modal
        $(document).on('click', '#delete', function () {
            if (comment_id_ !== null) {
                $.ajax({
                    url: '{{ action('Admin\Messages\UserMessage@delete') }}',
                    type: 'delete',
                    data: {
                        "comment_id": comment_id_,
                        "_token": _token
                    },
                    success: function (res__) {
                        var res = JSON.parse(res__);
                        $('div[data-id="' + comment_id_.toString() + '"]').remove();
                        _toastr(res['message'], "top-right", "success", false);
                        $('#myModal').modal('hide');
                    }
                })
            }

        });


        // when click on the delete button
        $(document).on('click', '.delete', function () {
            comment_id_ = $(this).attr('id');
        });
    </script>
@endsection