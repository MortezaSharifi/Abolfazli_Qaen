@extends('base.admin_base')
@section('title','سامانه پیامکی')
@section('sms_nav')
    class = "active"
@stop
@section('body')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="#" method="post">
                        <div class="form-group">
                            <label for="message" class="cols-sm-2 control-label color333333">
                                متن
                            </label>
                            <div class="cols-sm-10">
                                            <textarea class="form-control" id="message" style="min-height: 150px"></textarea>
                            </div>
                        </div>


                        <div class="form-group ">
                            <button type="button" id="send_msg" style="width: 20%"
                                    class="btn btn-success btn-md center-block">
                                ارسال
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
<script>
    $(document).on('click', '#send_msg', function () {

        var message = $('#message').val();
        var _token = $('meta[name="_token"]').attr('content');

        $.ajax({
            type: 'POST',
            url: "{{action('Admin\Messages\SmsController@post')}}",
            data: {
                "message": message,
                "_token": _token
            },
            success: function (_ret) {
                var ret = JSON.parse(_ret);

                if (ret['status'] === 'error') {
                    _toastr(ret['message']['E_message'], "top-right", "error", false);
                }
                console.log(ret);
                if (ret['status'] === 'success') {
                    _toastr(ret['message'], "top-left", "success", false);
                    $('#message').val('');
                }
            },
            error: function (e) {
                console.log(e);
            }
        })
    });
</script>
@endsection