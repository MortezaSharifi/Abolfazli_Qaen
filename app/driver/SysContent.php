<?php

/**
 * Created by PhpStorm.
 * User: salari
 * Date: 7/12/2017
 * Time: 4:31 PM
 */

namespace App\driver;

use App\Content;
use Illuminate\Database\Eloquent\Model;
use Mockery\Exception;
use Illuminate\Database\QueryException;
use App\Libraries;
use App\driver\SysMultimedia;

class SysContent extends Content
{
    private $id;
    private $caption;
    private $text;
    private $date;
    private $users_id;

    public function __construct($id = null, $users_id = null, $caption = null, $text = null, $date = null)
    {
        // call parent constructor
        parent::__construct();

        $this->id = $id;
        $this->caption = $caption;
        $this->text = $text;
        $this->date = $date;
        $this->users_id = $users_id;
    }


    public function getAll()
    {
        try {

            return content::all();

        } catch (QueryException $e) {
            return [];
        }

    }

    public function get_All_()
    {
        try {
            $cnt = \DB::table("type")->join('content', 'content.id', '=', 'type.content_id')->select()->where('type', '=', 'news')->orwhere('type', '=', 'notification')->orderBy('date', 'desc')->get();
            return $cnt;
        } catch (QueryException $e) {
            return [];
        }

    }


    public function getOne()
    {
        try {
            $_content_ = \DB::table("content")->select()->where('id', $this->id)->first();
            return $_content_;

        } catch (QueryException $e) {

            return false;
        }

    }

    public function getLast()
    {
        try {

            $data = Content::all();
            return collect($data)->last();
        } catch (QueryException $e) {
            return [];
        }

    }

    public function insert()
    {
        try {
            $post = new Content();
            $post->caption = $this->caption;
            $post->text = $this->text;
            $post->users_id = $this->users_id;
            $post->save();

            return $post->id;
        } catch (QueryException $e) {
            return false;
        }

    }

    public function deleteRecord()
    {
        try {
            $post = Content::find($this->id);

            $post->delete();
            return true;
        } catch (QueryException $e) {
            return false;
        }

    }

    public function updateRecord()
    {
        try {
            $post = \DB::table('content')->where('id', $this->id)->update(['caption' => $this->caption, 'text' => $this->text, 'users_id' => $this->users_id]);

            return $post;
        } catch (QueryException $e) {
            return false;
        }

    }


    public function getLimitNews()
    {
        try {

            return Content::join('type', 'content.id', '=', 'type.content_id')->orderBy('date', 'desc')->having('type', '=', 'news')->take(1)->get();
        } catch (QueryException $e) {
            return [];
        }

    }


    public function getLimitNotification()
    {
        try {

            return Content::join('type', 'content.id', '=', 'type.content_id')->orderBy('date', 'desc')->having('type', '=', 'notification')->take(5)->get();
        } catch (QueryException $e) {
            return [];
        }
    }

    public function getGalleryName()
    {
        try {

            return Content::join('type', 'content.id', '=', 'type.content_id')->join('multimedia', 'content.id', '=', 'multimedia.content_id')->orderBy('date', 'desc')->having('type', '=', 'gallery')->get();
        } catch (QueryException $e) {
            return [];
        }

    }
}