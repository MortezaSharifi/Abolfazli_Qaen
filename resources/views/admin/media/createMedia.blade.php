@extends('base.admin_base')
@section('title','ایجاد رسانه')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link href="{{ URL::asset("plugin/dropzone/min/dropzone.min.css") }}" rel="stylesheet" type="text/css"/>


@endsection
@section('news_nav')

    class = "active"
@stop
@section('body')

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div>
                        <!-- csrf token for security -->
                        <input name="_token" type="hidden" value="{{csrf_token()}}"/>

                        <div class="form-group">
                            <label for="caption">عنوان</label>
                            <input type="text" name="title" class="form-control" id="caption"
                                   placeholder="عنوان را وارد کنید ...">
                        </div>

                        <label class="radio-inline">
                            <input type="radio" name="media" value="sad" checked>  بخش عزاداری
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="media" value="Celebration">  بخش جشن و اعیاد
                        </label>

                        <label class="radio-inline">
                            <input type="radio" name="media" value="civil" checked> بخش عمران
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="media" value="etc">  بخش سایر
                        </label>

                        <label class="radio-inline">
                            <input type="radio" name="media" value="Eagerness" checked>  صوت مداحی
                        </label>

                        <label class="radio-inline">
                            <input type="radio" name="media" value="Sound"> صوت مولودی
                        </label>



                        <form enctype="multipart/form-data" method="post" style="margin: 15px 0;" id='my_dropzone'
                              class="dropzone dz-clickable">
                            <div class="dz-default dz-message"><span>فایل را از سیستم انتخاب یا بکشید و رها کنید</span>
                            </div>
                        </form>


                        <div name="reset" class="btn btn-danger">پاک کردن</div>
                        <div class="btn btn-success" id="send"> ثبت</div>

                    </div>

                </div>
            </div>
        </div>
    </div>

@stop
@section('js')
    <script src="{{ URL::asset('plugin/dropzone/min/dropzone.min.js') }}"></script>

    <!-- PAGE LEVEL SCRIPTS -->
    <script type="text/javascript">
        var ls_file_ids = [];

        function empty() {
            $("#caption").val('');
            ls_file_ids = [];
            $('.dz-preview').remove();
        }

        function setListFileId(data) {
            ls_file_ids.push(data);
        }

        //validation
        function newsValidationCustom() {
            var caption = $('#caption').val();

            // validation
            return !(caption.length < 1 );

        }
        //end

        function getGroup(){
            return $('input[name=media]').val();
        }

        function getLitFileID() {
            return JSON.stringify(ls_file_ids);
        }
        // dropzone
        var $dropzone = $('#my_dropzone');
        Dropzone.autoDiscover = false;
        $dropzone.dropzone({
            url: "{{action('Admin\Post\UploadController@post')}}",
            paramName: "file",
            addRemoveLinks: true,
            dictDefaultMessage: '<span class="text-center"><span class="font-lg visible-xs-block visible-sm-block visible-lg-block"><span class="font-lg"><i class="fa fa-caret-right text-danger"></i> Drop files <span class="font-xs">to upload</span></span><span>&nbsp&nbsp<h4 class="display-inline"> (Or Click)</h4></span>',
            dictResponseError: 'آپلود فایل با خطا مواجه شد.',
            dictFallbackMessage: 'مروگروب شما قابلیت کشیدن فایل و انداختن آن را پشتیبانی نمی کند.',
            parallelUploads: 1,
            dictCancelUpload: 'لغو آپلود',
            dictRemoveFile: 'حذف فایل',
            dictCancelUploadConfirmation: 'آیا شما مطمئن هستید از لغو آپلود فایل ؟',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                "Cache-Control": "",
                "X-Requested-With": ""
            },
            removedfile: function (_file) {
                var $fileElement = $(_file['previewElement']);
                var fileId = $fileElement.find('a').attr('data-id');
                var _token = $('meta[name="_token"]').attr('content');
                // delete file
                $.ajax({
                    type: 'POST',
                    url: "{{action('Admin\Post\DeleteFileController@post')}}",
                    data: {"fileId": fileId, "_token": _token},
                    success: function (_ret) {
                        var ret = JSON.parse(_ret);
                        $fileElement.remove();
                        //delete from ls-file-ids
                        if (ret.status == 'success'){
                            $fileElement.remove();
                            ls_file_ids = null
                        }else {

                        }
                        ls_fil_eids = $.grep(ls_file_ids, function (e) {
                            return e.fileId !== fileId
                        });
                        console.log(ls_file_ids)
//                        delete ls_file_ids[];

                    },
                    error: function (e) {
                        $fileElement.remove();

                    }
                })

            },
            success: function (_file, _ret) {
                var ret = JSON.parse(_ret);

                if (ret['status'] === 'success') {
                    var fileName = ret['fileName'];
                    var fileId = ret['fileId'];
                    var contentType = ret['contentType'];

                    // add file id to remove link
                    $(_file['previewElement']).find('a').attr('data-id', fileId.toString());

                    var file = {"fileName": fileName, "fileId": fileId, "contentType": contentType};
                    console.log(file);
                    setListFileId(file);
                } else {
                    console.log('file not allowed !!!');
                    var $fileElement = $(_file['previewElement']);
                    $fileElement.remove();

                }

            }
        });


        $(document).on('click', '#send', function () {
            if (!newsValidationCustom()) {
                alert('لطفا همه فیلدها را پر کنید');
                return;
            }

            var caption = $('#caption').val();
            var files = getLitFileID();
            var _token = $('meta[name="_token"]').attr('content');

            console.log(files);

            $.ajax({
                type: 'POST',
                url: "{{action('media\GalleryController@post')}}",
                data: {
                    "group": getGroup(),
                    "files": files,
                    "caption": caption,
                    "_token": _token
                },
                success: function (_ret) {
                    var ret = JSON.parse(_ret);
//                    $fileElement.remove();
                    ls_file_ids = [];
                    console.log(ret);

                    alert(ret['message']);
                    empty();

                },
                error: function (e) {
                    console.log(e)
                }
            })

        })


    </script>




@endsection