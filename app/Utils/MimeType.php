<?php
/**
 * Created by PhpStorm.
 * User: Shadow
 * Date: 25/07/2017
 * Time: 06:07 PM
 */

namespace App\Utils;


class MimeType
{
    public static $images = ['image/jpeg', 'image/png', 'image/gif', 'image/svg+xml', 'image/bmp', 'image/tiff', 'image/jpg'];
    public static $audios = ['audio/x-wav', 'audio/mpeg', 'audio/x-aac', 'audio/ogg', 'audio/flac'];
    public static $videos = ['video/mpeg', 'video/3gpp', 'video/x-msvideo', 'video/divx', 'video/x-flv', 'video/mp4', 'video/x-ms-wmv'];

}