<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class comment extends Model
{
    protected $table='comment';
    public $timestamps = false;
    protected $fillable = [
       'id', 'text', 'date', 'users_id', 'status'
    ];
}
