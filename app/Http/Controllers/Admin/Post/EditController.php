<?php
/**
 * Created by PhpStorm.
 * User: salari
 * Date: 9/4/2017
 * Time: 12:12 AM
 */

namespace App\Http\Controllers\Admin\Post;


use App\driver\SysMultimedia;
use App\driver\SysType;
use App\Http\Controllers\Base\Base;
use App\Http\Controllers\Controller;
use App\driver\SysContent;

use App\Multimedia;
use App\Utils\PasswordHashing;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Utils\Upload;
use App\Utils\MimeType;
use \View as View;
class EditController extends Base
{

    public function get()
    {
        return view('admin.Edit');

    }
    public function get_news($content_id){
        $sysContent = new SysContent($content_id);
        $one_news = $sysContent->getone();
        $sysType = new SysType(null, null, (int)$content_id);
        $sysMultimedia = new SysMultimedia();
        $pic = $sysMultimedia->get_pic($content_id);
        $types_list = $sysType->getTypesForContent();
        return view('admin.Edit',compact('one_news', 'types_list','pic'));

    }

    public function post()
    {
        // get data from client request
        $user_id = $this->request->session()->get('userid');
        $title = $this->request->input('caption');
        $news = $this->request->input('news');
        $notification = $this->request->input('notification');;
        $text = $this->request->input('text');
        $id_news = $this->request->input('id_news');
        $files = json_decode($this->request->input('files'),true);

        $objectContent = new SysContent((int)$id_news,$user_id,$title,$text  );
        $content_id = $objectContent->updateRecord();

        for($i=0; $i < count($files); $i++){
            $contentType = $files[$i]["contentType"];
            $fileName = $files[$i]["fileName"];
            $fileId = $files[$i]["fileId"];
            $objectMultiMedia = new SysMultimedia(null, $fileName, $id_news, $contentType, $fileId);
            $objectMultiMedia->updateRecord();
        }

        if ($news == "news"){
            $objectType = new SysType($id_news,'news',$content_id);
            $objectType->updateRecord();
        }
        if ($notification == "notification"){
            $objectType = new SysType($id_news,'notification',$content_id);
            $objectType->updateRecord();

        }




        return json_encode([
            "status"=>'success',
            "message"=>'اطلاعات با موفقیت ویرایش شد!'
        ]);
    }




}