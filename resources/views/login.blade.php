<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="_token" content="{{ csrf_token() }}"/>
    <title>SB Admin 2 - Bootstrap Admin Theme</title>

    <!-- Bootstrap Core CSS -->
    <link href={{url("css/bootstrap.min.css")}} rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href={{url("css/plugins/metisMenu/metisMenu.min.css")}} rel="stylesheet">

    <!-- Custom CSS -->
    <link href={{url("css/sb-admin-2.css")}} rel="stylesheet">

    <!-- Custom Fonts -->
    <link href={{url("css/font-awesome.min.css")}} rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src={{url("https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js")}}></script>
        <script src={{url("https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js")}}></script>
    <![endif]-->

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Sign In</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form">
                                <div class="form-group">
                                    <input class="form-control" placeholder="E-mail" id="username" type="text" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" id="password" type="password" value="">
                                </div>

                                <!-- Change this to a button or input when using this as a form -->
                                <button id="login" class="btn btn-lg btn-success btn-block">Login</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery Version 1.11.0 -->
    <script src={{url("js/jquery-1.11.0.js")}}></script>

    <!-- Bootstrap Core JavaScript -->
    <script src={{url("js/bootstrap.min.js")}}></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src={{url("js/metisMenu/metisMenu.min.js")}}></script>

    <!-- Custom Theme JavaScript -->
    <script src={{url("js/sb-admin-2.js")}}></script>
    <script>
        $(document).on('click', '#login', function () {

            var username = $('#username').val();
            var password = $('#password').val();
            var _token = $('meta[name="_token"]').attr('content');
            $.ajax({
                type: 'POST',
                url: "{{action('Site\LoginController@post')}}",
                data: {

                    "username": username,
                    "password": password,
                    "_token": _token

                },

                success: function (_ret) {
                    var ret = JSON.parse(_ret);

                    console.log(ret);



                },
                error: function (e) {
                    console.log(e);
                }
            })

        })
    </script>

</body>

</html>
