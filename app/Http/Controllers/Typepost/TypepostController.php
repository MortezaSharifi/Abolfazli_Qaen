<?php

namespace App\Http\Controllers\Typepost;

use App\Http\Controllers\Controller;
use App\driver\SysTypepost;
use App\Typepost;
use Illuminate\Http\Request;
use App\Http\Requests;

class TypepostController extends Controller
{
    public function get()
    {

        $object_type_post = new SysTypepost();
        $all_type_post = $object_type_post->getAll();
        $type_post = $object_type_post->getOne();
        return view('test',compact('all_type_post'));
    }

    public function post(Request $request)
    {
        // get data from client request
        $f = $request->input('f');
        if(empty($f)){
            return "error";
        }
        $objectTypepost = new SysTypepost(null,$f);
        $objectTypepost->insert();

        $objectTypepost1= new SysTypepost(4);
        $done = $objectTypepost1->deleteRecord();
        var_dump($done);
        return $f;
    }
}