<?php

/**
 * Created by PhpStorm.
 * User: salari
 * Date: 7/12/2017
 * Time: 4:31 PM
 */

namespace App\driver;

use App\Cost;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Mockery\Exception;

class SysCost
{
    private $id;
    private $value;
    private $name;


    public function __construct($id = null, $value = null, $name = null)
    {

        $this->id = $id;
        $this->value = $value;
        $this->name = $name;

    }

    public function getAll()
    {
        try {
            return  Cost::all();
        } catch (QueryException $e) {
            return [];
        }

    }

    public function checkWeep(){
        try{
        return Cost::find(2);
        }catch (QueryException $e){
            return false;
        }
    }
    public function checkCele(){
        try{
            return Cost::find(1);
        }catch (QueryException $e){
            return false;
        }
    }

    public function getOne()
    {
        try {
            return Cost::all()->where('id', $this->id);
        } catch (Exception $e) {
            return [];
        }

    }

    public function insert()
    {
        try {
            $_cost = new Cost();
            $_cost->id = $this->id;
            $_cost->value = $this->value;
            $_cost->name = $this->name;

            $_cost->save();

            return true;
        } catch (Exception $e) {
            return false;
        }

    }

    public function deleteRecord()
    {
        try {
            $_cost = Cost::find($this->id);
            $_cost->delete();
            return true;
        } catch (Exception $e) {
            return false;
        }

    }

    public function updateRecord()
    {
        try {
            $_cost = Cost::find($this->id);

            if (isset($this->value)) {
                $_cost->value = $this->value;
            }


            $_cost->save();


            return true;
        } catch (Exception $e) {
            return false;
        }

    }
}