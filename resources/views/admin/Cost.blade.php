@extends('base.admin_base')
@section('title','تعیین هزینه ها')
@section('body')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body" style="padding-top: 35px;">
                    <form class="form-horizontal" method="post">
                        <div class="form-group">
                            <label for="cost-weep" class="col-sm-2 col-sm-offset-2 control-label color333333">
                                هزینه ترحیم
                            </label>
                            <div class="col-sm-6  margin-bottom-10 ">
                                <div class="input-group">
                                    <span class="input-group-addon">ریال </span>
                                    <input type="number" class="form-control" name="cost-weep"
                                           @if($costs != 'empty')
                                                   value="{{$costs[1]['value']}}"
                                           @endif
                                           id="cost-weep" placeholder="مبلغ را وارد کنید"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="cost-cele" class="col-sm-2 col-sm-offset-2 control-label color333333">
                                هزینه جشن
                            </label>
                            <div class="col-sm-6  margin-bottom-10 ">
                                <div class="input-group">
                                    <span class="input-group-addon">ریال </span>
                                    <input type="number" class="form-control" name="cost-cele"
                                           @if($costs != 'empty')
                                           value="{{$costs[0]['value']}}"
                                           @endif
                                           id="cost-cele" placeholder="مبلغ را وارد کنید"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group ">
                            <button type="button" id="save" style="width: 20%"
                                    class="btn btn-success center-block">
                                ثبت
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <script>
        $(document).on('click', '#save', function () {

            var cost_weep = $('#cost-weep').val();
            var cost_cele = $('#cost-cele').val();
            var _token = $('meta[name="_token"]').attr('content');
            $.ajax({
                type: 'POST',
                url: "{{action('Admin\Cost\CostController@post')}}",
                data: {

                    "cost_weep": cost_weep,
                    "cost_cele": cost_cele,
                    "_token": _token
                },


                success: function (_ret) {
                    var ret = JSON.parse(_ret);


                    if (ret['status'] === 'error') {
                        _toastr(ret['message']['E_cost_weep'], "top-right", "error", false);
                        _toastr(ret['message']['E_cost_cele'], "top-right", "error", false);


                    }
                    console.log(ret);
                    if (ret['status'] === 'success') {
                        _toastr(ret['message']['cele-insert'], "top-left", "success", false);
                        _toastr(ret['message']['cele-update'], "top-left", "success", false);
                        _toastr(ret['message']['weep-insert'], "top-left", "success", false);
                        _toastr(ret['message']['weep-update'], "top-left", "success", false);
                    }
                },
                error: function (e) {
                    console.log(e);
                }
            })

        })
    </script>
@endsection