<?php

/**
 * Created by PhpStorm.
 * User: salari
 * Date: 9/14/2017
 * Time: 7:36 PM
 */
namespace App\Http\Controllers\media;

use App\driver\SysGallery;
use App\driver\SysMultimedia;
use App\driver\SysType;
use App\Http\Controllers\Base\Base;
use App\Http\Controllers\Controller;
use App\driver\SysContent;
use App\Multimedia;
use App\Utils\PasswordHashing;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Utils\Upload;
use App\Utils\MimeType;

class GalleryController extends Base
{
    function __construct(Request $request)
    {
        $type = Base::get_user_type();
        parent::__construct($request, $type,'ADMIN');
    }
    public function get(){

        return view('admin.media.createMedia');
    }


    public function get_(){
        return view('admin.media.deleteMedia');
    }
    public function post()
    {
        // get data from client request
        $title = $this->request->input('caption');
        $group = $this->request->input('group');
        $files = json_decode($this->request->input('files'),true);

        for($i=0; $i < count($files); $i++){
            $contentType = $files[$i]["contentType"];
            $fileName = $files[$i]["fileName"];
            $fileId = $files[$i]["fileId"];
            $objectMultiMedia = new SysGallery(null, $fileName,$contentType , $fileId, $title, $group);
            $objectMultiMedia->insert();
        }

        return json_encode([
            "status"=>'success',
            "message"=>'اطلاعات با موفقیت ثبت شد!'
        ]);
    }


}