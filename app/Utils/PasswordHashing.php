<?php
/**
 * Created by PhpStorm.
 * User: Shadow
 * Date: 28/07/2017
 * Time: 10:46 AM
 */

namespace App\Utils;


class PasswordHashing
{
    public static function makeSalt($length=12){
        $characters = "kajhsdkKHYIUL:J*&sdf^sdf*&@:**&^%@@@lkjasdfu234S2342552as!!dfsdDSA";
        $charactersLength = strlen($characters);
        $randomString = '';
        for($i=0; $i < $length; $i++){
            $randomString .= $characters[ rand( 0, $charactersLength - 1 ) ];
        }
        return $randomString;
    }

    public static function makePWDHash($username, $password, $salt=null){
        if($salt !== null){
            $salt = self::makeSalt();
        }
        $options = [
            'cost' => 11
        ];
        $h = password_hash($username, PASSWORD_DEFAULT, ['cost' => 15]);
        return $h;

    }

    public static function validPWD($username, $password, $hdb){
        $salt = explode(',',$hdb)[1];
        return  (string)$hdb === (string)self::makePWDHash($username, $password, $salt);
    }
}