<?php

namespace App\Http\Controllers\Usergroup;

use App\driver\SysUsergroup;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

class UsergroupController extends Controller
{
    public function get()
    {

        $object_user_group = new SysUsergroup();
        $all_user_group = $object_user_group->getAll();
        $user_group1 = $object_user_group->getOne();
        return view('test',compact('all_user_group'));
    }

    public function post(Request $request)
    {
        // get data from client request
        $f = $request->input('f');

        $objectUsergroup = new SysUsergroup(null,$f);
        $objectUsergroup->insert();

        $objectUsergroup = new SysUsergroup();
        $objectUsergroup->deleteRecord();
        return $f;
    }


}