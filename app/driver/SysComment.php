<?php

/**
 * Created by PhpStorm.
 * User: salari
 * Date: 7/12/2017
 * Time: 4:31 PM
 */

namespace App\driver;

use App\comment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class SysComment
{
    private $id;
    private $text;
    private $date;
    private $users_id;
    private $status;

    public function __construct($id = null, $text = null, $date = null,
                                $users_id = null, $status = null)
    {

        $this->id = $id;
        $this->text = $text;
        $this->date = $date;
        $this->users_id = $users_id;
        $this->status = $status;

    }

    public function getAll()
    {
        try {
            return Comment::all();
        } catch (Exception $e) {
            return [];
        }

    }

    public function getOne()
    {
        try {
            return Comment::all()->where('id', $this->id);
        } catch (Exception $e) {
            return [];
        }

    }

    public function get_msg()
    {
        try {
            return Comment::join('users', 'comment.users_id', '=', 'users.id')->where('users_id', '=', $this->users_id)->orderBy('date', 'desc')->get();
        } catch (QueryException $e) {
            return [];
        }

    }

    public function get_user_msg()
    {
        try {
            return Comment::join('users', 'comment.users_id', '=', 'users.id')->where('status', '=', $this->status)->
            select(DB::raw('users.*, comment.*,users.id as uid'))->orderBy('date', 'desc')->get();
        } catch (QueryException $e) {
            return [];
        }

    }
    public function get_three_msg()
    {
        try {
            return Comment::join('users', 'comment.users_id', '=', 'users.id')->where('status', '=', $this->status)->
            select(DB::raw('users.*, comment.*,users.id as uid'))->orderBy('date', 'desc')->take(3)->get()->toArray();
        } catch (QueryException $e) {
            return [];
        }

    }

    public function insert()
    {
        try {
            $_comment = new Comment;
            $_comment->text = $this->text;
            $_comment->users_id = $this->users_id;
            $_comment->status = $this->status;

            $_comment->save();

            return true;
        } catch (Exception $e) {
            return false;
        }

    }

    public function deleteRecord()
    {
        try {
            $_comment = Comment::find($this->id);

            $_comment->delete();
            return true;
        } catch (QueryException $e) {
            return false;
        }

    }

    public function updateRecord()
    {
        try {
            $_comment = Comment::find($this->id);

            if (isset($this->text)) {
                $_comment->text = $this->text;
            }
            if (isset($this->users_id)) {
                $_comment->users_id = $this->users_id;
            }
            if (isset($this->status)) {
                $_comment->status = $this->status;
            }

            $_comment->save();


            return true;
        } catch (Exception $e) {
            return false;
        }

    }
}