@extends('base.site_base')
@section('title','پرداخت')
@section('content')
    <div class="container">
        <div class="col-lg-8 col-lg-offset-2">
            <div class="panel panel-default padding-30">
                <h2 class="text-center">پرداخت</h2>
                <hr>
                <form class="form-horizontal" method="post" action="#">
                    @if($type == null)
                        <div class="form-group">
                            <label for="name" class="col-sm-2 col-sm-offset-2 control-label color333333">نام</label>
                            <div class="col-sm-6  margin-bottom-10  ">
                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-user fa"
                                                                                       aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="fname" id="name"
                                           placeholder="نام خود را وارد کنید"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="lname" class="col-sm-2 col-sm-offset-2 control-label color333333">نام
                                خانوادگی</label>
                            <div class="col-sm-6 margin-bottom-10 ">
                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-user fa"
                                                                                       aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="lname" id="lname"
                                           placeholder="نام خانوادگی خود را وارد کنید"/>
                                </div>
                            </div>
                        </div>
                    @endif

                    <div class="form-group">
                        <label for="type" class="col-sm-2 col-sm-offset-2 control-label color333333">
                            نوع پرداختی
                        </label>
                        <div class="col-sm-6  margin-bottom-10 ">
                            <label>
                                <select class="form-control" id="type" name="type">
                                    <option value="n">نذورات</option>
                                    <option value="k">کمک به بازسازی</option>
                                    <option value="g">جشن</option>
                                    <option value="t">ترحیم</option>
                                    <option value="s">سایر</option>
                                </select>
                            </label>
                        </div>
                    </div>
                    <div id="explanation" class="form-group" style="display: none">
                        <label for="explain" class="col-sm-2 col-sm-offset-2 control-label color333333">
                            توضیحات
                        </label>
                        <div class="col-sm-6  margin-bottom-10  ">
                            <textarea id="explain" class="width-100-darsad"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="amount" class="col-sm-2 col-sm-offset-2 control-label color333333">
                            مبلغ
                        </label>
                        <div class="col-sm-6  margin-bottom-10 ">
                            <div class="input-group">
                                <span class="input-group-addon">ریال </span>
                                <input type="number" class="form-control" name="amount"
                                       id="amount" placeholder="مبلغ را وارد کنید"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group ">
                        <button type="button" id="pay"
                                class="btn red_btn btn-lg btn-block login-button center-block width-40-darsad">
                            پرداخت
                        </button>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        var w_cost, c_cost;
        var _token = $('meta[name="_token"]').attr('content');
        $.ajax({
            type: 'POST',
            url: "{{action('Site\Payment\PaymentController@getCost')}}",
            data: {

                "_token": _token
            },

            success: function (_ret) {
                var ret = JSON.parse(_ret);
                console.log(ret);
                $('#type').on('change', function () {
                    if ($("#type").find("option:selected").text() === 'ترحیم') {
                        $("#explanation").css('display', 'none');
                        w_cost = ret['costs'][1]['value'];
                        $("#amount").val(w_cost);
                        $("#amount").prop('disabled', true);

                    }
                    if ($("#type").find("option:selected").text() === 'جشن') {
                        $("#explanation").css('display', 'none');
                        c_cost = ret['costs'][0]['value'];
                        $("#amount").val(c_cost);
                        $("#amount").prop('disabled', true);

                    }
                    if ($("#type").find("option:selected").text() === 'نذورات'
                        || $("#type").find("option:selected").text() === 'کمک به بازسازی') {
                        $("#explanation").css('display', 'none');
                        $("#amount").val('');
                        $("#amount").prop('disabled', false);
                    }
                    if ($("#type").find("option:selected").text() === 'سایر') {
                        $("#explanation").css('display', 'block');
                        $("#amount").val('');
                        $("#amount").prop('disabled', false);
                    }

                });

            },
            error: function (e) {
                console.log(e);
            }
        });

        $(document).on('click', '#pay', function () {
            var fname = $("#name").val();
            var lname = $("#lname").val();
            var type = $("#type").find("option:selected").val();
            var text = $("#explain").val();
            var amount = $("#amount").val();
            $.ajax({
                type: 'POST',
                url: "{{action('Site\Payment\PaymentController@post')}}",
                data: {
                    "fname": fname,
                    "lname": lname,
                    "type": type,
                    "text": text,
                    "amount": amount,
                    "_token": _token
                },

                success: function (_ret) {
                    var ret = JSON.parse(_ret);
                    console.log(ret);
                    if (ret['status'] === 'success') {
                        _toastr(ret['message']['E_amount'], "top-right", "success", false);
                        $("#name").val('');
                        $("#lname").val('');
                        $("#explain").val('');
                        $("#amount").val('');

                    }
                    if (ret['status'] === 'error') {
                        _toastr(ret['message']['E_amount'], "top-right", "error", false);
                    }
                },
                error: function (e) {
                    console.log(e);
                }
            });
        });
    </script>
@endsection