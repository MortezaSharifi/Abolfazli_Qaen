<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property null value
 * @property null users_id
 * @property null type
 * @property null text
 * @property null fname
 * @property null lname
 * @property null transaction_id
 * @property null status
 */
class payment extends Model
{
    protected $table='payment';
    public $timestamps = false;
    protected $fillable = [
        'id', 'date', 'value', 'users_id', 'type', 'text', 'fname', 'lname', 'transaction_id', 'status'
    ];
}
