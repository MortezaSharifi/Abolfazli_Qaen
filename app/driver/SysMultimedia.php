<?php

/**
 * Created by PhpStorm.
 * User: salari
 * Date: 7/12/2017
 * Time: 4:31 PM
 */

namespace App\driver;


use App\Multimedia;
use App\gallery;
use App\Utils\Upload;
use Illuminate\Database\Eloquent\Model;
use Mockery\Exception;
use Illuminate\database\QueryException;
use Illuminate\Support\Facades\DB;

class SysMultimedia
{
    private $id;
    private $name;
    private $content_id;
    private $content_type;
    private $file_id;

    public function __construct($id = null, $name = null, $content_id = null,
                                $content_type = null, $file_id = null)
    {

        $this->id = $id;
        $this->name = $name;
        $this->content_id = $content_id;
        $this->content_type = $content_type;
        $this->file_id = $file_id;

    }

    public function getAll()
    {
        try {
            return Multimedia::all();
        } catch (QueryException $e) {
            return [];
        }

    }

    public function get_pic_by_id($content_id)
    {
        try {
            $M = DB::table("content")->join('multimedia', 'multimedia.content_id', '=', 'content.id')->select('file_id')->where('content.id', $content_id)->first();
            if ($M == null) {
                return array('status' => '300');
            } else {
                return array('status' => '350', 'pic' => $M);
            }
        } catch (QueryException $e) {
            return false;
        }
    }

    public function get_pic($content_id)
    {
        try {
            $M = DB::table("multimedia")->select('file_id', 'content_type')->where('content_id', $content_id)->first();
            if ($M == null) {
                return array('status' => '300');
            } else {
                $pic = $M->file_id;
                return array('status' => '350', 'pic' => $pic);

            }
        } catch (QueryException $e) {
            return false;
        }
    }


    public function get_file_id()
    {
        try {
//            $Mm=DB::select('select file_id from multimedia where id = (select max(id) from multimedia)');
            $Mm = DB::table("multimedia")->select()->orderBy("id", "desc")->first();
            return $Mm;

        } catch (QueryException $e) {
            return false;
        }
    }


    public function getFileDetails(){
        try {

            //$ret = Multimedia::all()->where('file_id', $this->file_id);
            $ret = DB::table('multimedia')->select()->where('file_id', '=', $this->file_id)->first();
            if(count($ret) > 0){
                return $ret;
            }else{
                return DB::table('gallery')->select()->where('file_id', '=', $this->file_id)->first();
                //return gallery::all()->where('file_id', $this->file_id)[0];
            }
        }
        catch (QueryException $e) {
            return [];
        }

    }

    public function getOne($content_id = null)
    {
        if ($content_id == null) {
            try {
                return Multimedia::all()->where('id', $this->id);
            } catch (QueryException $e) {
                return [];
            }
        } else {
            try {
                return \DB::table('multimedia')->where('content_id', $content_id)->get();
            } catch (QueryException $e) {
                return [];
            }
        }


    }

    public function insert()
    {
        try {
            $_Multimedia = new Multimedia;
            $_Multimedia->name = $this->name;
            $_Multimedia->content_id = $this->content_id;
            $_Multimedia->content_type = $this->content_type;
            $_Multimedia->file_id = $this->file_id;


            $_Multimedia->save();

            return true;
        } catch (QueryException $e) {
            return false;
        }

    }

    public function deleteRecord()
    {
        try {
            $_Multimedia = Multimedia::find($this->id);

            $_Multimedia->delete();
            return true;
        } catch (QueryException $e) {
            return false;
        }

    }

    public function updateRecord()
    {
        try {
            $old_news =$this->getOne($this->content_id);
            $_Multimedia = \DB::table('multimedia')->where('content_id', $this->content_id)->update(['name' => $this->name, 'file_id' => $this->file_id, 'content_type' => $this->content_type]);
            if ($_Multimedia == 1) {
                if ($old_news[0]->file_id == $this->file_id){

                }else{
                    $manageFile = new Upload();
                    $path_pic = 'uploaded/'.$old_news[0]->file_id;
                    $delete_file = $manageFile->delete_image($path_pic);
                }
            }
            return $_Multimedia;
        } catch (QueryException $e) {
            return false;
        }

    }

    public function deleteContentRecord()
    {
        try {
            $_Multimedia = Multimedia::where('content_id', $this->id);

            $_Multimedia->delete();
            return true;
        } catch (QueryException $e) {
            return false;
        }

    }
}