<?php

namespace App\Http\Controllers\Admin\User;
use App\Http\Controllers\Base\Base;
use App\Http\Controllers\Controller;
use App\driver\SysUsers;
use Illuminate\Http\Request;

class UserController extends Base
{
    function __construct(Request $request)
    {
        $type = Base::get_user_type();
        parent::__construct($request, $type, 'ADMIN');
    }

    public function get()
    {

        $object_users = new SysUsers();
        $all_users = $object_users->getAll_user();
        return view('admin.users',compact('all_users'));
    }


}