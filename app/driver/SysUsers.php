<?php

/**
 * Created by PhpStorm.
 * User: salari
 * Date: 7/12/2017
 * Time: 4:31 PM
 */

namespace App\driver;

use App\Users;
use Illuminate\Database\Eloquent\Model;
use Mockery\Exception;
use Illuminate\Database\QueryException;

class SysUsers
{
    private $id;
    private $fname;
    private $lname;
    private $phone;
    private $email;
    private $address;
    private $username;
    private $password;
    private $user_group_id;

    public function __construct($id = null, $username = null, $fname = null, $lname = null, $phone = null, $email = null, $address = null,
                                $password = null, $user_group_id = null)
    {
        $this->id = $id;
        $this->fname = $fname;
        $this->lname = $lname;
        $this->phone = $phone;
        $this->email = $email;
        $this->address = $address;
        $this->username = $username;
        $this->password = $password;
        $this->user_group_id = $user_group_id;
    }

    public function getAll()
    {
        try {
            return Users::all();
        } catch (Exception $e) {
            return [];
        }

    }
    public function getAll_user()
    {
        try {
            return Users::where('user_group_id', '=', 1)->get();
        } catch (Exception $e) {
            return [];
        }

    }
    public function getCount_user()
    {
        try {
            return Users::where('user_group_id', '=', 1)->count();
        } catch (Exception $e) {
            return [];
        }

    }

    public function getOne()
    {
        try {
            return Users::where('id','=', $this->id)->firstOrFail();
        } catch (QueryException $e) {
            return [];
        }

    }
    public function get_by_uname()
    {
        try {
            return Users::where('username', '=', $this->username)->first();
        } catch (QueryException $e) {
            return false;
        }

    }
    public function get_type()
    {
        try {
            return Users::where('users.id','=',$this->id)->join('user_group','users.user_group_id','=','user_group.id')->select('user_group.type')->firstorfail();
        } catch (Exception $e) {
            return [];
        }

    }

    public function insert()
    {
        try {
            $user = new Users;
            $user->fname = $this->fname;
            $user->lname = $this->lname;
            $user->phone = $this->phone;
            $user->email = $this->email;
            $user->address = $this->address;
            $user->username = $this->username;
            $user->password = $this->password;
            $user->user_group_id = $this->user_group_id;
            $user->save();

            return $user->id;
        } catch (QueryException $e){
            return false;
        }

    }

    public function deleteRecord()
    {
        try {
            $user = Users::find($this->id);

            $user->delete();
            return true;
        } catch (Exception $e) {
            return false;
        }

    }

    public function updateRecord()
    {
        try {
            $user = Users::find($this->id);

            if (isset($this->lname)) {
                $user->lname = $this->lname;
            }
            if (isset($this->fname)) {
                $user->fname = $this->fname;
            }
            if (isset($this->phone)) {
                $user->phone = $this->phone;
            }
            if (isset($this->email)) {
                $user->email = $this->email;
            }
            if (isset($this->address)) {
                $user->address = $this->address;
            }
            if (isset($this->username)) {
                $user->username = $this->username;
            }
            if (isset($this->password)) {
                $user->password = $this->password;
            }
            if (isset($this->user_group_id)) {
                $user->user_group_id = $this->user_group_id;
            }

            $user->save();


            return true;
        } catch (QueryException $e) {
            return false;
        }

    }


    public function get_number()
    {
        try {
            return Users::select('phone')->get();
        } catch (QueryException $e) {
            return false;
        }
    }
}