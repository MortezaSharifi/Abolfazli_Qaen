@extends('base.admin_base')
@section('title','تغییر گذرواژه')
@section('setting_nav')
    class = "active"
@stop
@section('body')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-md-8 col-md-offset-2">
                        <form class="form-horizontal" method="post" action="#">
                            <div class="form-group">
                                <label for="password" class="cols-sm-2 control-label color333333">گذرواژه
                                    قبلی</label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-lock fa-lg"
                                                                                       aria-hidden="true"></i></span>
                                        <input type="password" class="form-control" name="password"
                                               id="password" placeholder="گذرواژه قبلی خود را وارد کنید"/>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="new_pass" class="cols-sm-2 control-label color333333">گذرواژه
                                    جدید</label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-lock fa-lg"
                                                                                       aria-hidden="true"></i></span>
                                        <input type="password" class="form-control" name="new_pass"
                                               id="new_pass" placeholder=" گذرواژه جدید خود را وارد کنید"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="conf_pass" class="cols-sm-2 control-label color333333">تکرار گذرواژه
                                    جدید</label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-lock fa-lg"
                                                                                       aria-hidden="true"></i></span>
                                        <input type="password" class="form-control" name="conf_pass"
                                               id="conf_pass"
                                               placeholder=" گذرواژه جدید خود را مجددا وارد کنید"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <button type="button" id="change_pass"
                                        class="btn btn-success  btn-md center-block ">
                                    تغییر گذرواژه
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script>
        $(document).on('click', '#change_pass', function () {

            var password = $('#password').val();
            var new_pass = $('#new_pass').val();
            var conf_pass = $('#conf_pass').val();
            var _token = $('meta[name="_token"]').attr('content');

            $.ajax({
                type: 'POST',
                url: "{{action('Admin\ChangePass\ChangePassController@post')}}",
                data: {

                    "password": password,
                    "new_pass": new_pass,
                    "conf_pass": conf_pass,
                    "_token": _token
                },


                success: function (_ret) {
                    var ret = JSON.parse(_ret);


                    if (ret['status'] === 'error') {
                        $('input[type = "password"]').val('');
                        _toastr(ret['message']['E_password'], "top-right", "error", false);
                        _toastr(ret['message']['E_new_pass'], "top-right", "error", false);
                        _toastr(ret['message']['E_conf_pass'], "top-right", "error", false);
                        _toastr(ret['message']['E_match'], "top-right", "error", false);


                    }
                    console.log(ret);
                    if (ret['status'] === 'success') {
                        _toastr(ret['message'], "top-left", "success", false);
                        $('input[type = "password"]').val('');


                    }
                },
                error: function (e) {
                    console.log(e);
                }
            })

        })
    </script>
@endsection