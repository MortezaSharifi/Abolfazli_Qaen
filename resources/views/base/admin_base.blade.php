<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="_token" content="{{ csrf_token() }}"/>
    <title>@yield('title')</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
{{--    <link href="{{ URL::asset('css/bootstrap-rtl.css') }}" rel="stylesheet">--}}
<!-- MetisMenu CSS -->
    <link href="{{ URL::asset('css/plugins/metisMenu/metisMenu.min.css') }}" rel="stylesheet">
    <!-- Timeline CSS -->
    <link href="{{ URL::asset('css/plugins/timeline.css') }}" rel="stylesheet">
@yield('css')
<!-- Custom CSS -->

    <link href="{{ URL::asset('css/sb-admin-2.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/common.css') }}">
    <link rel="stylesheet" href="{{URL::asset('plugin/toastr/toastr.css')}}">
    <!-- Custom Fonts -->
    <link href="{{ URL::asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="{{ URL::asset('https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js') }}"></script>
    <script src="{{ URL::asset('https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js') }}"></script>
    <![endif]-->
</head>

<body>

<div id="wrapper">
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><b>سامانه مدیریت هیئت ابوالفضلی</b></a>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-left">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-envelope fa-fw"></i> <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-messages" id="msg">

                </ul>
                <!-- /.dropdown-messages -->
            </li>
            <!-- /.dropdown -->


            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">

                    <li><a href="<?= Url('admin/changepass') ?>"><i class="fa fa-gear fa-fw"></i> تغییر گذرواژه</a>
                    </li>
                    <li class="divider"></li>
                    <li><a id="logout"><i class="fa fa-sign-out fa-fw"></i> خروج</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">

                    <li>
                        <a @yield('dash_nav') href="<?= Url('admin/dashboard') ?>"><i class="fa fa-dashboard fa-fw"></i>
                            داشبورد</a>
                    </li>
                    <li>

                        <a @yield('up_slider_nav') href="<?= Url('admin/upslider') ?>"><i
                                    class="fa fa-upload fa-fw"></i>
                            آپلود عکس اسلایدر
                        </a>

                    <li>
                        <a href="#"><i class="fa fa-newspaper-o fa-fw"></i> اطلاعیه ها و اخبار<span
                                    class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a @yield('news_nav') href="<?= Url('/admin/posts/get') ?>">ایجاد اطلاعیه یا خبر
                                    جدید</a>
                            </li>
                            <li>
                                <a @yield('showNews_nav') href="<?= Url('/admin/showcontent') ?>">نمایش اطلاعیه ها و
                                    اخبار</a>
                            </li>

                        </ul>

                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-archive fa-fw"></i>رسانه ها<span
                                    class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a @yield('media_nav') href="<?= Url('admin/media/createMedia') ?>">ایجاد رسانه</a>
                            </li>
                            <li>
                                <a @yield('media_nav') href="<?= Url('admin/media/deleteMedia') ?>">نمایش رسانه</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a @yield('report_nav') href="<?= Url('admin/report') ?>"><i class="fa fa-table fa-fw"></i>
                            گزارشات</a>
                    </li>
                    <li>
                        <a @yield('users_nav') href="<?= Url('admin/users') ?>"><i class="fa fa-users fa-fw"></i>
                            اعضا</a>
                    </li>

                    <li>
                        <a href="#"><i class="fa fa-comment fa-fw"></i>پیام ها<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a @yield('message_nav') href="<?= Url('admin/message') ?>">پیام کاربران</a>
                            </li>
                            <li>
                                <a @yield('sms_nav') href="<?= Url('admin/sms') ?>">سامانه پیامکی</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a @yield('cost_nav') href="<?= Url('admin/cost') ?>"><i class="fa fa-money fa-fw"></i>تعیین هزینه ها</a>
                    </li>
                    <li>
                        <a @yield('setting_nav') href="<?= Url('admin/changepass') ?>"><i class="fa fa-gear fa-fw"></i>تغییر گذرواژه</a>
                    </li>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">@yield('title')</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        @yield('body')

    </div>


</div>
<!-- /#wrapper -->

<!-- jQuery Version 1.11.0 -->
<script src="{{ URL::asset('js/jquery-1.11.0.js') }}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="{{ URL::asset('js/metisMenu/metisMenu.min.js') }}"></script>

<!-- Custom Theme JavaScript -->
<script src="{{ URL::asset('js/sb-admin-2.js') }}"></script>

<script src="{{URL::asset('plugin/toastr/toastr.js')}}"></script>
<script src="{{URL::asset('js/func_toastr.js')}}"></script>

@yield('js')

<script>
    $.ajax({
        type: 'POST',
        url: "{{action('Admin\Messages\UserMessage@lastMessages')}}",
        data: {
            "_token": $('meta[name="_token"]').attr('content')
        },

        success: function (_ret) {
            var ret = JSON.parse(_ret);
            if (ret['status'] !== 'error') {
                var message = '';
                for (x in ret['username']) {
                    message += "<li><a><div>" +
                        "<strong>" + ret['username'][x] + "</strong>" +
                        "<span class='pull-left text-muted'><em>" + ret['date'][x] + "</em></span>" +
                        "</div><div>" + ret['text'][x] +

                        "</div></a></li>" + "<li class='divider'></li>";
                }
                message += "<li> <a class='text-center' href='<?= Url('admin/message') ?>'> <i class='fa fa-eye'></i> <strong>مشاهده پیام ها</strong> </a> </li>";
                document.getElementById("msg").innerHTML = message;
            }else {
                document.getElementById("msg").innerHTML = "<div style='text-align: center; padding: 10px'><strong>" + 'پیامی وجود ندارد' + "</strong></div>";
            }
            console.log(ret);

        },
        error: function (e) {
            console.log(e);
        }
    });
    $(document).on('click', '#logout', function () {

        var _token = $('meta[name="_token"]').attr('content');

        $.ajax({
            type: 'POST',
            url: "{{action('Site\LoginController@logout')}}",
            data: {
                "_token": _token
            },

            success: function (_ret) {
                var ret = JSON.parse(_ret);
                if (ret['status'] === 'success') {
                    location.reload();
                }

                console.log(ret);

            },
            error: function (e) {
                console.log(e);
            }
        })

    })
</script>
</body>

</html>




