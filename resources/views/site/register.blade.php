@extends('base.site_base')

@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="{{URL::asset('plugin/toastr/toastr.css')}}">
@endsection

@section('title','ثبت نام')
@section('content')
    <div class="container">
        <div class="row main">

            <div class="main-login main-center">

                <form class="form-horizontal">
                    <div class="panel-heading">
                        <div class="panel-title text-center">
                            <h1 class="title">ثبت نام</h1>
                            <hr/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="cols-sm-2 control-label">نام</label>
                        <div class="cols-sm-10 ">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" id="fname"
                                       placeholder="نام خود را حتما فارسی وارد کنید"/>

                            </div>
                            <label id="E_fname" class="hidden"></label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="lname" class="cols-sm-2 control-label">نام خانوادگی</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" id="lname"
                                       placeholder="نام خانوادگی خود را حتما فارسی وارد کنید"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="username" class="cols-sm-2 control-label">نام کاربری</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" id="username"
                                       placeholder="نام کاربری خود را وارد کنید"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password" class="cols-sm-2 control-label">گذرواژه</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"
                                                                   aria-hidden="true"></i></span>
                                <input type="password" class="form-control" id="password"
                                       placeholder="گذرواژه خود را وارد کنید"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="confirm" class="cols-sm-2 control-label">تایید گذواژه</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"
                                                                   aria-hidden="true"></i></span>
                                <input type="password" class="form-control" id="confirm"
                                       placeholder="مجددا گذرواژه خود را وارد کنید"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group ">
                        <button type="button" id="send" class="btn red_btn btn-lg btn-block login-button">ثبت نام
                        </button>
                    </div>
                    <div class="login-register">
                        <a href="<?= Url('login') ?>" data-toggle="modal"
                           data-target="#login-modal">ورود</a>
                    </div>
                </form>

            </div>
        </div>
    </div>

    @endsection
@section('js')
    <script  src="{{URL::asset('plugin/toastr/toastr.js')}}"></script>

    <script>

        $(document).on('click', '#send', function () {

            var fname = $('#fname').val();
            var lname = $('#lname').val();
            var username = $('#username').val();
            var password = $('#password').val();
            var conf_pass = $('#confirm').val();
            var _token = $('meta[name="_token"]').attr('content');

            $.ajax({
                type: 'POST',
                url: "{{action('Site\Register\RegisterController@post')}}",
                data: {
                    "fname": fname,
                    "lname": lname,
                    "username": username,
                    "password": password,
                    "conf_pass": conf_pass,
                    "_token": _token
                },

                success: function (_ret) {
                    var ret = JSON.parse(_ret);

                    console.log(ret);

                    if (ret['status'] === 'error') {
                        _toastr(ret['message']['E_fname'],"top-right","error",false);
                        _toastr(ret['message']['E_lname'],"top-right","error",false);
                        _toastr(ret['message']['E_username'],"top-right","error",false);
                        _toastr(ret['message']['E_pass'],"top-right","error",false);
                        _toastr(ret['message']['E_con_pass'],"top-right","error",false);
                        _toastr(ret['message']['E_match'],"top-right","error",false);
                        _toastr(ret['message']['E_exist_user'],"top-right","error",false);
                        $('input[type = "password"]').val('');

                    }
                    if (ret['status'] === 'success') {
                        $('input[type="text"],input[type = "password"]').val('');
                        _toastr("ثبت نام با موفقیت انجام شد.","top-left","success",false);
                        setTimeout(function () {
                            window.location.href = "/profile"
                        }, 5500);

                    }
                },
                error: function (e) {
                    console.log(e);
                }
            })

        })

    </script>
@endsection