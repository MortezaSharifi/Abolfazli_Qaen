<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;


/**
 * @property null fname
 * @property null phone
 * @property null address
 * @property null username
 * @property null password
 * @property null user_group_id
 * @property null lname
 * @property null email
 */
class Users extends Model
{
    protected $table = 'users';
    public $timestamps = false;
    protected $fillable = [
        'id', 'fname', 'lname','phone', 'username', 'password', 'user_group_id', 'address', 'email'
    ];

}

