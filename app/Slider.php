<?php
/**
 * Created by PhpStorm.
 * User: Shadow
 * Date: 25/07/2017
 * Time: 02:22 PM
 */
namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $table = 'slider';
    public $timestamps = false;

}

