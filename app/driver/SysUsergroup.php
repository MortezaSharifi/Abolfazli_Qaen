<?php

/**
 * Created by PhpStorm.
 * User: salari
 * Date: 7/12/2017
 * Time: 4:31 PM
 */

namespace App\driver;
use App\Usergroup;
use Illuminate\Database\Eloquent\Model;
use Mockery\Exception;


class SysUsergroup
{
    private $id;
    private $type;


    public function __construct($id = null, $type=null)
    {
        // call parent constructor
        parent::__construct();

        $this->id = $id;
        $this->type = $type;

    }

    public function getAll()
    {
        try {
            return Usergroup::all();
        } catch (Exception $e) {
            return [];
        }

    }

    public function getOne()
    {
        try {
            return Usergroup::all()->where('id', $this->id);
        } catch (Exception $e) {
            return [];
        }

    }

    public function insert()
    {
        try {
            $user_group= new Usergroup();
            $user_group->type = $this->type;
            $user_group->save();

            return true;
        } catch (Exception $e) {
            return false;
        }

    }

    public function deleteRecord()
    {
        try {
            $user_grope = Usergroup::find($this->id);

            $user_grope->delete();
            return true;
        } catch (Exception $e) {
            return false;
        }

    }

    public function updateRecord()
    {
        try {
            $user_group = Usergroup::find($this->id);


            $user_group->save();


            return true;
        } catch (Exception $e) {
            return false;
        }

    }
}