<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property null users_id
 * @property null text
 * @property null date
 * @property null caption
 */
class Content extends Model
{
    protected $table='content';
    public $timestamps = false;
    protected $fillable = [
       'id', 'caption','text', 'date', 'users_id'
    ];

}