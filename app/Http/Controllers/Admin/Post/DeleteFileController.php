<?php

namespace App\Http\Controllers\Admin\Post;

use App\Http\Controllers\Controller;
use App\driver\SysContent;
use App\driver\SysMultimedia;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Utils\Upload;
use App\Utils\MimeType;
use Illuminate\Support\Facades\Input;


class DeleteFileController extends Controller
{

    public function post(Request $request)
    {
        $fileId = $request->input('fileId');
        $path = 'uploaded/' . $fileId;
        $sysUpload = new Upload();
        $delete_file = $sysUpload->delete_image($path);
        // delete form db if exist
        if ($delete_file['status']) {
            return json_encode(["status" => 'success']);

        }else{
            return json_encode(["status" => 'error']);
        }
    }


}