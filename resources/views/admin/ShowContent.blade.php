@extends('base.admin_base')
@section('title','نمایش اطلاعیه ها و اخبار')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link href="{{URL::asset('css/plugins/dataTables.bootstrap.css')}}" rel="stylesheet">
@endsection

@section('showNews_nav')
    class = "active"
@stop

@section('body')
<meta charset="utf-8">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="table-responsive" >

                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">

                            <thead>
                            <tr>
                                <th>عنوان</th>
                                <th>تاریخ</th>
                                <th>عملیات</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($allContent as $list)
                                <tr data-id="{{ $list->id  }}">
                                    <td>{{$list->caption}}</td>
                                    <td> {{$list->date }} </td>
                                    <td class="text-center">
                                        <a href="<?= URL('/admin/Edit', $list->id)?>"><button class="btn btn-warning edit" ><span class="fa fa-edit "></span> ویرایش</button></a>
                                        <button class="btn btn-danger delete" id="{{$list->id}}" data-toggle="modal" data-target="#myModal"><span class="fa fa-remove"></span> حذف</button>

                                    </td>

                                </tr>
                            @endforeach
                            
                            </tbody>

                        </table>

                        <!-- Modal -->
                        <div class="modal fade closeModal" id="myModal" role="dialog">
                            <div class="modal-dialog modal-sm">
                                <div class="modal-content ">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">حذف</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>آیا از حذف اطمینان دارید؟</p>
                                    </div>
                                    <div class="modal-footer" >
                                        <button type="button" class="btn btn-primary" data-dismiss="modal" ><span class="fa fa-mail-forward"></span>لغو</button>
                                        <button type="button" id="delete" class="btn btn-danger" ><span class="fa fa-trash"></span>حذف</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.table-responsive -->


                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="{{url::asset('js/jquery/jquery.dataTables.min.js')}}"></script>
    <script src="{{url::asset('js/bootstrap/dataTables.bootstrap.min.js')}}"></script>

    <!-- PAGE LEVEL SCRIPTS -->
    <script type="text/javascript">
        var _token = $('meta[name="_token"]').attr('content');
        var news_id_ = null;
        // when click on the delete button over modal
        $(document).on('click', '#delete', function () {

            if (news_id_ !== null) {
                $.ajax({
                    url: '{{ action('Admin\Post\ContentController@delete') }}',
                    type: 'POST',
                    data: {
                        "news_id": news_id_,
                        "_token": _token
                    },
                    success: function (res__) {
                        var  res = JSON.parse(res__);
                        $('tr[data-id="'+ news_id_.toString() +'"]').remove();
                        console.log(res__);
                        alert(res['message']);
                        $('#myModal').modal('hide');
                    },
                    error: function(e){
                        console.log(e)
                    }
                })
            }

        });


        // when click on the delete button
        $(document).on('click', '.delete', function(){
            news_id_ = $(this).attr('id');
        });

        $(document).ready(function() {
            $('#dataTables-example').dataTable();
        });

    </script>
@endsection