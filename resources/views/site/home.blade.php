@extends('base.site_base')
@section('title','هیئت ابوالفضلی')

@section('content')

    <!-- ==================start content body section=============== -->


    <!-- ==================start slider================ -->
    <div class="container">

        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators ">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" id="maincarousel">

                <div class="item active">

                    <img src="{{asset('img/stuff_img1.jpg')}}" alt="تصویر اول">
                    <div class="carousel-caption">
                        <h3>عنوان اول</h3>
                        <p>متن آزمایشی</p>
                    </div>
                </div>

                <div class="item">
                    <img src="{{asset('img/featured_img1.jpg')}}" alt="تصویر دوم">
                    <div class="carousel-caption">
                        <h3>عنوان دوم</h3>
                        <p>متن آزمایشی</p>
                    </div>
                </div>

                <div class="item">
                    <img src="{{asset('img/stuff_img1.jpg')}}" alt="تصویر سوم">
                    <div class="carousel-caption">
                        <h3>عنوان سوم</h3>
                        <p>متن آزمایشی </p>
                    </div>
                </div>

            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <!-- ==================End slider================ -->

    <div class="container">
        <div class="row">
            <!-- start left bar content -->
            <div class=" col-sm-12 col-md-6 col-lg-6">
                <div class="row">
                    <div class="leftbar_content">
                        <h2>اخبار جدید</h2>
                        <!-- start single stuff post -->
                        @if($news == false)
                            <div > <h2>اخبار موجود نیست</h2> </div>
                        @else
                            @foreach($news as $item)
                                <div class="single_stuff wow fadeInDown">
                                    <div class="single_stuff_img">
                                        @if($item->file == null)
                                            <a href="<?= Url('news', $item->content_id)?>"><img
                                                        src="{{asset('img/stuff_img1.jpg')}}" style="width:100%;height: 400px"
                                                        alt="img"></a>

                                        @else
                                            <a href="<?= Url('news', $item->content_id)?>"><img
                                                        src="{{ action('Admin\Post\DownloadController@get', $item->file->file_id) }}" style="width: 100%;height: 400px"
                                                        alt="img"></a>
                                        @endif

                                    </div>

                                    <div class="single_stuff_article">
                                        <div class="single_sarticle_inner">
                                            <a class="stuff_category" href="#"><strong></strong></a>
                                            <div class="stuff_article_inner">
                                                <span class="stuff_date"><strong>{{$item->date}}</strong></span>
                                                <h2>
                                                    <a href="<?= URL("news", $item->content_id)?>">{{$item->caption}}</a>

                                                </h2>
                                                {!! substr(($item->text),0,105) !!}

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            @endforeach
                            <div class="stuffpost_paginatinonarea wow slideInRight">
                                <ul class="newstuff_pagnav">
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <!-- End left bar content -->


            <div class="col-sm-6 col-md-4 col-lg-4 col-lg-offset-1 col-md-offset-1">
                <div class="row">
                    <div class="rightbar_content">
                        <!-- start Popular Posts -->


                        <div class="single_blog_sidebar wow fadeInUp">
                            <h2>اطلاعیه ها</h2>
                            @foreach($listNotification as $item)
                                <ul class="middlebar_nav wow">
                                    <li>
                                        <a href="#" class="mbar_thubnail"><img alt="img"
                                                                               src="{{asset('img/logo.png')}}"></a>
                                        <a href="#" class="mbar_title">{{$item->caption}}</a>
                                    </li>

                                </ul>
                            @endforeach
                        </div>
                        <!-- End Popular Posts -->

                        <!-- start featured post -->

                        <div class="single_blog_sidebar wow fadeInUp">
                            <h2>تصویر روز</h2>
                            <ul class="featured_nav">
                                <li>

                                    @if($_file_id == null)
                                        <a class="featured_img" href="<?= URL('gallery') ?> ">
                                            <img src="{{asset('img/stuff_img1.jpg')}}" alt="img">
                                           برای مشاهده روی تصویر کلیک کنید
                                        </a>
                                    @else
                                        <a class="featured_img" href="<?= URL('gallery') ?> ">
                                            <img src="/content/download/{{$_file_id->file_id}}" alt="img">
                                            برای مشاهده روی تصویر کلیک کنید
                                        </a>
                                    @endif

                                </li>
                            </ul>

                        </div>

                        <!-- End featured post -->

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{ URL::asset('js/pagination.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var all_page = Math.floor({{ $all_page }});
            var page_now = Math.floor({{ $page_now }});
            paginate(".newstuff_pagnav", all_page, page_now, 7, "/index");
        })
    </script>
@endsection
<!-- ==================End content body section=============== -->


