<?php
/**
 * Created by PhpStorm.
 * User: Shadow
 * Date: 29/07/2017
 * Time: 11:01 AM
 */

namespace App\Utils;


class ValidationForm
{
    function requiredField($data)
    {
        if (empty($data))
            return false;

        return true;
    }

    function req_alpha_Field($data)
    {
        if (preg_match('/^[پچجحخهعغفقثصضشسیبلاتنمکگوئدذرزطظژؤإآأءًٌٍَُِّ\s]+$/u', $data) and !empty($data)) {
            return true;
        }
        return false;
    }

    function checkEmail($email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return false;
        }
        return true;
    }

    function matchPassword($pass, $conf_pass)
    {
        if (($pass != $conf_pass) or empty($pass)) {
            return false;
        }
        return true;
    }
}