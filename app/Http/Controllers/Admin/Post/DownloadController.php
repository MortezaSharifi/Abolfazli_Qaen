<?php

namespace App\Http\Controllers\Admin\Post;

use App\driver\SysMultimedia;
use App\Http\Controllers\Controller;
use App\driver\SysContent;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Mockery\Exception;


class DownloadController extends Controller
{
    public function get(Request $request)
    {
        $fileId = $request->route('fileId');
        //  return file id details
        $obj = new SysMultimedia(null,null,null,null,$fileId);
        $array = $obj->getFileDetails();

        $r = json_decode(json_encode($array), true);
        if(count($r) < 1){
            return "Error: File not found.";
        }
        if (array_key_exists('file_type', $r)){
            $contentType = (string)$r['file_type'];
        }else{
            $contentType = (string)$r['content_type'];
        }

        $fileName = (string)$r['name'];

        $attachment_location = public_path() . env('UPLOAD_PATH', '/uploaded') . '/' . $fileId;
        if (file_exists($attachment_location)) {
            header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
            header("Cache-Control: public"); // needed for internet explorer
            header("Content-Type: " . $contentType);
           header("Content-Length:" . filesize($attachment_location));
            header("Content-Disposition: attachment; filename=" . $fileName);
            return readfile ($attachment_location);

        } else {
            return "Error: File not found.";
        }

    }

}