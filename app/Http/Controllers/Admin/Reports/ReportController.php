<?php
/**
 * Created by PhpStorm.
 * User: Shadow
 * Date: 08/09/2017
 * Time: 08:55 AM
 */

namespace App\Http\Controllers\Admin\Reports;


use App\driver\SysPayment;
use App\Http\Controllers\Base\Base;
use Illuminate\Http\Request;
use App\Libraries;

class ReportController extends Base
{
    function __construct(Request $request)
    {
        $type = Base::get_user_type();
        parent::__construct($request, $type, 'ADMIN');
    }

    public function get(){

        $payment = new SysPayment();
        $details = $payment->getAll();
        include(app_path() . '\Libraries\jdf.php');
        for ($i = 0; $i < count($details); $i++){
            $details[$i]['date'] = Libraries\jdate(" i : H j F Y", strtotime($details[$i]['date']));

            if ($details[$i]['type'] == 'n')
                $details[$i]['type'] = 'نذورات';
            if ($details[$i]['type'] == 'k')
                $details[$i]['type'] = 'کمک به بازسازی';
            if ($details[$i]['type'] == 'g')
                $details[$i]['type'] = 'جشن';
            if ($details[$i]['type'] == 't')
                $details[$i]['type'] = 'ترحیم';
            if ($details[$i]['type'] == 's')
                $details[$i]['type'] = 'سایر';

        }

        return view('admin.report', compact('details'));
    }
}