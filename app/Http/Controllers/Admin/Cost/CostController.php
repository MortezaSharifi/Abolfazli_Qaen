<?php
/**
 * Created by PhpStorm.
 * User: Shadow
 * Date: 04/09/2017
 * Time: 03:39 PM
 */

namespace App\Http\Controllers\Admin\Cost;


use App\driver\SysCost;
use App\Http\Controllers\Base\Base;
use App\Utils\ValidationForm;
use Illuminate\Http\Request;

class CostController extends Base
{
    function __construct(Request $request)
    {
        $type = Base::get_user_type();
        parent::__construct($request, $type, 'ADMIN');
    }

    public function get()
    {
        $getCost = new SysCost();
        $costs = $getCost->getAll();
        if ($costs->isEmpty())
            $costs = 'empty';
        return view('admin.Cost', compact('type','costs'));
    }

    public function post()
    {

        $cost_cele = $this->request->input('cost_cele');
        $cost_weep = $this->request->input('cost_weep');

        $valid = new ValidationForm();
        if (!$valid->requiredField($cost_cele)) {
            $err['E_cost_cele'] = 'هزینه جشن را وارد کنید';
        } else $err['E_cost_cele'] = '';

        if (!$valid->requiredField($cost_weep)) {
            $err['E_cost_weep'] = 'هزینه ترحیم را وارد کنید';
        } else $err['E_cost_weep'] = '';

        if ($valid->requiredField($cost_cele) and $valid->requiredField($cost_weep)) {
            $c_cost = new SysCost(1, $cost_cele, 'جشن');
            $res = $c_cost->checkCele();
            $ret['cele-insert'] ='';
            $ret['cele-update'] ='';
            $ret['weep-insert'] = '';
            $ret['weep-update'] ='';
            if ($res) {
                $c_cost->updateRecord();
                $ret['cele-update'] = 'هزینه جشن بروزرسانی شد';

            } else {
                $c_cost->insert();
                $ret['cele-insert'] = 'هزینه جشن ذخیره شد';
            }
            $w_cost = new SysCost(2, $cost_weep, 'ترحیم');
            $res = $w_cost->checkWeep();
            if ($res) {
                $w_cost->updateRecord();
                $ret['weep-update'] = 'هزینه ترحیم بروزرسانی شد';
            } else {
                $w_cost->insert();
                $ret['weep-insert'] = 'هزینه ترحیم ذخیره شد';
            }
            return json_encode([
                "status" => 'success',
                "message" => $ret
            ]);
        }


        return json_encode([
            "status" => 'error',
            "message" => $err
        ]);
    }
}