<?php
/**
 * Created by PhpStorm.
 * User: Shadow
 * Date: 09/09/2017
 * Time: 02:45 PM
 */

namespace App\Http\Controllers\Admin\Dashboard;


use App\driver\SysCost;
use App\driver\SysPayment;
use App\driver\SysUsers;
use App\Http\Controllers\Base\Base;
use Illuminate\Http\Request;
use App\Libraries;

class DashboardController extends Base
{
    function __construct(Request $request)
    {
        $type = Base::get_user_type();
        parent::__construct($request, $type, 'ADMIN');
    }

    public function get(){

        $cost = new Syscost();
        $cost_w = $cost->checkWeep();
        $cost_c = $cost->checkCele();

        $obj_user = new SysUsers();
        $count = $obj_user->getCount_user();

        $obj_payment = new SysPayment();
        $total = $obj_payment->total_pay();
        $last_pay = $obj_payment->five_last();
        include(app_path() . '\Libraries\jdf.php');
        for ($i = 0; $i < count($last_pay); $i++){
            $last_pay[$i]['date'] = Libraries\jdate(" i : H j F Y", strtotime($last_pay[$i]['date']));

            if ($last_pay[$i]['type'] == 'n')
                $last_pay[$i]['type'] = 'نذورات';
            if ($last_pay[$i]['type'] == 'k')
                $last_pay[$i]['type'] = 'کمک به بازسازی';
            if ($last_pay[$i]['type'] == 'g')
                $last_pay[$i]['type'] = 'جشن';
            if ($last_pay[$i]['type'] == 't')
                $last_pay[$i]['type'] = 'ترحیم';
            if ($last_pay[$i]['type'] == 's')
                $last_pay[$i]['type'] = 'سایر';

        }

        return view('admin.dashboard', compact( 'last_pay', 'cost_w', 'cost_c', 'count', 'total'));

    }
}